# Practical Data Engineering  

Illustrating practical examples of Data Engineering solutions.
  

### Database / ETL examples  

[1. Abstracting SQL with SQLAlchemy (PostgreSQL + PGAdmin4 + SQLAlchemy)](./examples/postgres_pgadmin_sqlalchemy/Readme.md)  

[2. A containerized ETL (ArangoDB and Python-Arango)](./examples/containerized_etl/Readme.md)    


### A first Services Stack  
[3. Run containerized tasks on your stack](./examples/train\_predict/Readme.md)  

[4. A different task, a larger stack](./examples/djia_etl/Readme.md)  

### Using MLflow  
[5. Integrating MLflow into a model training script](./examples/using_mlflow/using_mlflow.md)    


### Airflow for orchestration / scheduling
[6. An Airflow and related services stack](./examples/adding_airflow/readme.md)  

x. Running an ML pipeline as an Airflow DAG  
