## A Tiny Services Stack 
the stack is tiny, not the services... they're huge.


#### Machine Learning with Experiment Tracking  

In the previous example we loaded water potability data into an ArangoDB database collection. Now let's go a step further and use that data to train a predictive model and track our experiment with MLflow. This gives us a chance to bring up multiple services at once. In the [services_stack](./services\_stack) directory are two docker-compose YAML files. We could bring them up separately with  
```
$ docker-compose -f docker-compose-arangodb.yml up -d
```  

and

```
$ docker-compose -f docker-compose-mlflow.yml up -d
```

but as we add more services to our stack, this would get unwieldy. We might forget to bring up a service, and then, when bringing the services down (that does happen on occasion) we'd have to remember each one again. In addition to that, many containerized services need volumes mounted on the host; we need to think about ownership and permissions of these directories.

One way to handle this is to encapsulate the directory creation and the docker commands in a [single shell script](./services-stack) (I'm using bash). Then we we can simply do  

```
$ bash services-stack up -d  
```  

to bring the services up, and  

```
$ bash services-stack down
```  

to bring them down.  

Here's the script:

```bash
#!/bin/bash

# create volumes directories to prevent root from owning them
[[ "$1" == "up" ]] \
    && mkdir -p $PWD/volumes/mlflow/postgres \
    && mkdir -p $PWD/volumes/arango-etl

# bring up/down the services defined in the docker-compose files
docker-compose -f $PWD/docker-compose-mlflow.yml \
               -f $PWD/docker-compose-arangodb.yml \
               $@
```

#### The Services User Interfaces

When the stack is brought up, we should have available a UI for ArangoDB and one for MLflow. Let's look at the ArangoDB UI first by connecting to [http://localhost:1234](http://localhost:1234). If followed along with the previous example [containerized_etl](../containerized\_etl/Readme.md), then this should look familiar. The username and password can be found in the associated docker-compose YAML file.   

Now let's open the MLflow UI at [http://localhost:5000](http://localhost:5000). We haven't yet begun tracking an experiment yet, so it's not very interesting at present.


#### Containerized Tasks

Our two services are waiting to be used; let's load the database with the containerized ETL we used previously. Move into the containerized_etl directory and do this:

```
$ bash build_run.sh
``` 

Check the UI to verify that the database was created and that it contains a collection with documents containing our data.  

The next task, containerized_ml, is not something we've seen before, so move into the directory and then into the app subdirectory. There's only one file, [predict.py](./containerized\_ml/app/predict.py).  

```python
'''
Train a k-nearest neighbors classifier to predict "water potability".
(following this Kaggle code: https://www.kaggle.com/code/manavkarthikeyan/water-quality-classification-knn-with-eda)
'''

import os

# next two lines quiet an annoying matplotlib warning
import tempfile
os.environ["MPLCONFIGDIR"] = tempfile.gettempdir()

from pathlib import Path

# quiet annoying pandas warnings
import warnings
warnings.catch_warnings()
warnings.simplefilter('ignore')

import numpy as np
from arango import ArangoClient
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

import mlflow
import mlflow.sklearn
#from mlflow.tracking import MlflowClient


# get the environment variables
db_url = os.getenv("DB_URL")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")
experiment_name = os.getenv("EXPERIMENT_NAME")


##########################################################
# Set up mlflow
##########################################################
mlflow.set_tracking_uri("http://localhost:5000")

experiment_id = mlflow.get_experiment_by_name(experiment_name)
if experiment_id is None:
    experiment_id = mlflow.create_experiment(experiment_name, artifact_location=data_dir)
else:
    experiment_id = experiment_id.experiment_id
mlflow.set_experiment(experiment_name)
##########################################################

with mlflow.start_run(experiment_id=experiment_id) as run:

    run_id = run.run_id = run.info.run_uuid
    
    # read documents from the "potability" collection of the ArangoDB "water" database
    client = ArangoClient(hosts=db_url)
    db = client.db(db_name, username=db_user, password=db_password) 
    potability = db.collection('potability')
    rows = []
    for doc in potability:
        rows.append(doc)
    df = pd.DataFrame.from_dict(rows, orient='columns')
    X = df.drop('Potability', axis=1)
    X = X.drop('_key', axis=1)
    X = X.drop('_id', axis=1)
    X = X.drop('_rev', axis=1)
    y = df['Potability']
    
    
    # build the classifier
    train_X, test_X, train_y, test_y = train_test_split(X,y,test_size=0.1)
    
    # standardize the data before model fitting
    scaler = RobustScaler()
    train_X = scaler.fit_transform(train_X)
    test_X = scaler.fit_transform(test_X)
    train_y = scaler.fit_transform(train_y.values.reshape(-1,1))
    test_y = scaler.fit_transform(test_y.values.reshape(-1,1))
    
    # build and fit the model
    nb_neighbors = 50
    params = {'n_neighbors':[i for i in range(1,nb_neighbors+1)]}
    clf = KNeighborsClassifier()
    clf1 = GridSearchCV(clf,params)
    clf1.fit(train_X,train_y)
    
    # final model
    model = KNeighborsClassifier(n_neighbors=clf1.best_params_['n_neighbors'])
    model.fit(train_X,train_y)
    score = accuracy_score(model.predict(test_X),test_y)
    print("accuracy_score:", score)
    
    plt.rcParams['figure.figsize'] = [7, 7]
    cm = confusion_matrix(model.predict(test_X),test_y)
    cmpath = os.path.join(data_dir, "confusion_matrix")
    np.save(cmpath, cm)
    
    sns.heatmap(cm, linewidth = 1,annot=True)
    pngname = "confusion_matrix.png"
    plt.savefig(os.path.join(data_dir, pngname))

    mlflow.log_param("nb_neighbors", nb_neighbors)
    mlflow.log_metric("score", score)
    mlflow.log_artifact(os.path.join(data_dir, pngname))
    mlflow.sklearn.log_model(sk_model=model,
                       artifact_path="sk_models",
                       registered_model_name="knn-classifier-model")
```

There's a bit to look through in the file above, so here's the summary:  

* After the imports, the environment variables are read which were passed into the container via the [build\_run\_predict.sh](./containerized_ml/build\_run\_predict.sh) script.  
* Set up tracking for our MLflow experiment.  
* Read the documents from the database and structure the data for training our classifier model.  
* Train the model.  
* Log a parameter, a metric, an artifact, and the model itself using mlflow.  


I ran build\_run\_predict.sh a few times, using a different value for nb_neighbors each time. Here is what the UI looks like:  

![alt text](./images/mlflow_ui.png "mlflow_ui")  

In the code, I set the artifact\_location variable to be data\_dir, which is defined in the build\_run\_predict.sh script to be a directory in my home dir named mlflow. That's where the logged confusion\_matrix.png should be written and the model stored. Looking at the directory tree under mlflow: 

![alt text](./images/host_mlflow_dir.png "mlflow\_dirs")  

Notice there there are three directories, with the long hash-value names, and inside is the artifact and the model.  

#### Summary  

This was a simple example, yet it illustrates how to get your system stack up an running and how to execute tasks which depend on it. We executed the tasks from individually from bash scripts, but the next time we run tasks, they will be run from the orchestration and monitoring tool Airflow. That's when we'll be in the big leagues... or at least using the tools that they use.  
