#!/bin/bash

# build
IMAGENAME="water_quality_etl"
TAG="0.0.1"
docker build -t $IMAGENAME:$TAG .

# run
HOSTDATA="/home/sylvanix/1210SRD/Practical_Data_Engineering/containerized_etl/water_quality"
docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=http://localhost:1234 \
    --env DB_NAME=water \
    --env DB_USER=root \
    --env DB_PASSWORD=adbpwd \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
