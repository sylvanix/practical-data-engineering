"""
An ETL (Extract, Transform, Load) example which reads in data from a source file, cleans it, and loads it in a database.
"""

import os
from pathlib import Path
import pandas as pd
from arango import ArangoClient


# Get the environment variables made available to the container
db_url = os.getenv("DB_URL")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")


#####################################################
# DATABASE CREATION & CONNECTION
####################################################
# Initialize the client for ArangoDB.
client = ArangoClient(hosts=db_url)

# Connect to the "_system" database as root user.
sys_db = client.db("_system", username=db_user, password=db_password)

# Create the "water" database if it does not exist
if not sys_db.has_database(db_name):
    sys_db.create_database(db_name)

# Connect to the "water" database as root user.
db = client.db(db_name, username=db_user, password=db_password)

# Create the "potability" collection if it does not exist
if db.has_collection('potability'):
    potability = db.collection('potability')
else:
    potability = db.create_collection('potability')


#####################################################
# ETL
####################################################
# (EXTRACT) Read the data from the csv
data_path = Path(data_dir) / "water_potability.csv"
df = pd.read_csv(data_path)

# (TRANSFORM) Handle the missing values
null_cols = ['ph','Sulfate','Trihalomethanes']
mask1 = df['Potability']==0
mask2 = df['Potability']==1
for col in null_cols:
    if col!='Sulfate':
        df[col].fillna(df[col].mean(), inplace=True)
    else:
        df.loc[mask1,col] = df.loc[mask1,col].fillna(df.loc[mask1, col].mean())
        df.loc[mask2,col] = df.loc[mask2,col].fillna(df.loc[mask2, col].mean())

# (LOAD) Write each record (row) to the database collection
dfstring = df.to_string(header=True,
                  index=False,
                  index_names=False).split('\n')
rows = [ele.split() for ele in dfstring]
header = rows[0]
rows = rows[1:]
row_dict = {}
for row in rows:
    for ii in range(len(header)):
        row_dict[header[ii]] = row[ii]
    # insert this row's values into the collection as a new document
    potability.insert(row_dict)
