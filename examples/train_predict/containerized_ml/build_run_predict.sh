#!/bin/bash

# build
IMAGENAME="predict_potability"
TAG="0.0.1"
docker build -t $IMAGENAME:$TAG .

# run
HOSTDATA="/home/sylvanix/mlflow"
EXPNAME="water-potability"
docker run \
    --net=host \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=http://localhost:1234 \
    --env DB_NAME=water \
    --env DB_USER=root \
    --env DB_PASSWORD=adbpwd \
    --env DATA_DIR=/data \
    --env EXPERIMENT_NAME=${EXPNAME} \
    $IMAGENAME:$TAG
