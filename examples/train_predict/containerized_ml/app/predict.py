'''
Train a k-nearest neighbors classifier to predict "water potability".
(following this Kaggle code: https://www.kaggle.com/code/manavkarthikeyan/water-quality-classification-knn-with-eda)
'''

import os

# next two lines quiet an annoying matplotlib warning
import tempfile
os.environ["MPLCONFIGDIR"] = tempfile.gettempdir()

from pathlib import Path

# quiet annoying pandas warnings
import warnings
warnings.catch_warnings()
warnings.simplefilter('ignore')

import numpy as np
from arango import ArangoClient
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import RobustScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

import mlflow
import mlflow.sklearn
#from mlflow.tracking import MlflowClient


# get the environment variables
db_url = os.getenv("DB_URL")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")
experiment_name = os.getenv("EXPERIMENT_NAME")


##########################################################
# Set up mlflow
##########################################################
mlflow.set_tracking_uri("http://localhost:5000")

experiment_id = mlflow.get_experiment_by_name(experiment_name)
if experiment_id is None:
    experiment_id = mlflow.create_experiment(experiment_name, artifact_location=data_dir)
else:
    experiment_id = experiment_id.experiment_id
mlflow.set_experiment(experiment_name)
##########################################################

with mlflow.start_run(experiment_id=experiment_id) as run:

    run_id = run.run_id = run.info.run_uuid
    
    # read documents from the "potability" collection of the ArangoDB "water" database
    client = ArangoClient(hosts=db_url)
    db = client.db(db_name, username=db_user, password=db_password) 
    potability = db.collection('potability')
    rows = []
    for doc in potability:
        rows.append(doc)
    df = pd.DataFrame.from_dict(rows, orient='columns')
    X = df.drop('Potability', axis=1)
    X = X.drop('_key', axis=1)
    X = X.drop('_id', axis=1)
    X = X.drop('_rev', axis=1)
    y = df['Potability']
    
    
    # build the classifier
    train_X, test_X, train_y, test_y = train_test_split(X,y,test_size=0.1)
    
    # standardize the data before model fitting
    scaler = RobustScaler()
    train_X = scaler.fit_transform(train_X)
    test_X = scaler.fit_transform(test_X)
    train_y = scaler.fit_transform(train_y.values.reshape(-1,1))
    test_y = scaler.fit_transform(test_y.values.reshape(-1,1))
    
    # build and fit the model
    nb_neighbors = 50
    params = {'n_neighbors':[i for i in range(1,nb_neighbors+1)]}
    clf = KNeighborsClassifier()
    clf1 = GridSearchCV(clf,params)
    clf1.fit(train_X,train_y)
    
    # final model
    model = KNeighborsClassifier(n_neighbors=clf1.best_params_['n_neighbors'])
    model.fit(train_X,train_y)
    score = accuracy_score(model.predict(test_X),test_y)
    print("accuracy_score:", score)
    
    plt.rcParams['figure.figsize'] = [7, 7]
    cm = confusion_matrix(model.predict(test_X),test_y)
    cmpath = os.path.join(data_dir, "confusion_matrix")
    np.save(cmpath, cm)
    
    sns.heatmap(cm, linewidth = 1,annot=True)
    pngname = "confusion_matrix.png"
    plt.savefig(os.path.join(data_dir, pngname))

    mlflow.log_param("nb_neighbors", nb_neighbors)
    mlflow.log_metric("score", score)
    mlflow.log_artifact(os.path.join(data_dir, pngname))
    mlflow.sklearn.log_model(sk_model=model,
                       artifact_path="sk_models",
                       registered_model_name="knn-classifier-model")
