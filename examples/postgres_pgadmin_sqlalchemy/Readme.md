## PostgreSQL and PGAdmin4  
#### Containerized with persistent volumes


Crucial to Data Engineering is getting the data into a usable structure and then loading it into a database. Let's look at how to model a tiny dataset with the Python library SQLAlchemy and load it into a PostgreSQL database which we can inspect in a browser UI.  


#### How to set up  
Create a project directory with volume directories for Postgres and Pgadmin. The pgadmin volume dir needs to be owned by uid = 5050 and in the group with gid = 5050 in order for its files to be written and accessed there.  
```  
$ git clone https://gitlab.com/sylvanix/practical-data-engineering  
$ cd practical-data-engineering/postgres_pgadmin_sqlalchemy  
$ mkdir -p db-project/volumes  
$ cd db-project/volumes  
$ mkdir postgres pgadmin
$ sudo chown 5050 pgadmin && sudo chgrp 5050 pgadmin
$ cd ..  

```  

Let's look at the docker-compose file [docker-compose-pg.yml](./docker-compose-pg.yml):     


```docker-compose
version: '3.7'  

services:  

  db:  
    container_name: postgres_container  
    image: postgres:latest  
    restart: unless-stopped  
    environment:  
      POSTGRES_USER: sylvanix  
      POSTGRES_PASSWORD: passwd  
      POSTGRES_DB: practical  
    volumes:  
       - ./volumes/postgres:/var/lib/postgresql/data  
    ports:  
      - 5432:5432  

  pgadmin:  
    container_name: pgadmin_container  
    image: dpage/pgadmin4  
    restart: unless-stopped  
    environment:  
      PGADMIN_DEFAULT_EMAIL: sylvanix@propitious.ai  
      PGADMIN_DEFAULT_PASSWORD: passwd  
    volumes:  
       - ./volumes/pgadmin:/var/lib/pgadmin  
    ports:  
      - "5050:80"  
```  

 
##### Pull the images and run the containers:  

```bash 
$ docker-compose -f docker-compose-pg.yml up  -d
```  

After the images are pulled, and the containers built, you should see the following:
```bash
Creating postgres_container ... done
Creating pgadmin_container  ... done
```  

Check the status with ```docker ps```  
```bash
sylvanix@kepler64b:postgres_pgadmin_sqlalchemy$ docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED          STATUS          PORTS                                            NAMES
e5fe3cc241fb   dpage/pgadmin4    "/entrypoint.sh"         52 seconds ago   Up 50 seconds   443/tcp, 0.0.0.0:5050->80/tcp, :::5050->80/tcp   pgadmin_container
78f72e72195c   postgres:latest   "docker-entrypoint.s…"   52 seconds ago   Up 50 seconds   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp        postgres_container
```  

Now you can go to your browser and sign into the PGAdmin4 UI at **localhost:5050**  

......


#### Create a table and insert some rows there.  
We could do this from the pgadmin UI, but let's use sqlalchemy to model a tiny dataset. Let's use the magic numbers in the linux reboot.h file. On my Debian system it's here: ```/usr/include/linux/reboot.h```, and it look's like this:  

```c
/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */
#ifndef _LINUX_REBOOT_H
#define _LINUX_REBOOT_H

/*
 * Magic values required to use _reboot() system call.
 */

#define	LINUX_REBOOT_MAGIC1	0xfee1dead
#define	LINUX_REBOOT_MAGIC2	672274793
#define	LINUX_REBOOT_MAGIC2A	85072278
#define	LINUX_REBOOT_MAGIC2B	369367448
#define	LINUX_REBOOT_MAGIC2C	537993216


/*
 * Commands accepted by the _reboot() system call.
 *
 * RESTART     Restart system using default command and mode.
 * HALT        Stop OS and give system control to ROM monitor, if any.
 * CAD_ON      Ctrl-Alt-Del sequence causes RESTART command.
 * CAD_OFF     Ctrl-Alt-Del sequence sends SIGINT to init task.
 * POWER_OFF   Stop OS and remove all power from system, if possible.
 * RESTART2    Restart system using given command string.
 * SW_SUSPEND  Suspend system using software suspend if compiled in.
 * KEXEC       Restart system using a previously loaded Linux kernel
 */

#define	LINUX_REBOOT_CMD_RESTART	0x01234567
#define	LINUX_REBOOT_CMD_HALT		0xCDEF0123
#define	LINUX_REBOOT_CMD_CAD_ON		0x89ABCDEF
#define	LINUX_REBOOT_CMD_CAD_OFF	0x00000000
#define	LINUX_REBOOT_CMD_POWER_OFF	0x4321FEDC
#define	LINUX_REBOOT_CMD_RESTART2	0xA1B2C3D4
#define	LINUX_REBOOT_CMD_SW_SUSPEND	0xD000FCE2
#define	LINUX_REBOOT_CMD_KEXEC		0x45584543



#endif /* _LINUX_REBOOT_H */
```  

Why are magic values required to reboot the system? Whence come they? The answer to the first question seems to be that passing a pair of these magic numbers to reboot ensures that you really want to reboot the system. The answer to the second question can be understood by converting the decimal MAGIC2 numbers to hexidecimal. MAGIC2, for example in hexidecimal is 28121969 which is Linus's birthday in ddmmyyyy format. The other three numbers are the birthdays of his children. Okay, it's interesting, but not really valuable data for us, but this is only meant to be a toy dataset anyway. Let's model it like so: [magic\_numbers\_model.py](./magic_numbers_model.py):  
```python
'''
Example of data model, using SqlAlchemy with a PostgreSQL database 
'''

import sqlalchemy as sa 
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert
import psycopg2 
 

Base = declarative_base() 

 
class MagicNumber(Base):

    __tablename__ = 'numbers'
    magic_number = sa.Column('magic_number', sa.Integer, primary_key=True)
    person = sa.Column('person', sa.String) 
    relation = sa.Column('relation', sa.String) 
    birthdate = sa.Column('birthdate', sa.String)

    def __init__(self, magic_number, person, relation): 
        self.magic_number = magic_number
        self.person = person 
        self.relation = relation
        # convert integer magic_number to hex to reveal birthdate
        self.birthdate = hex(magic_number)
 
    def __repr__(self): 
        return "<MagicNumber({}, {}, {}, {})>".format(
            self.magic_number, self.person, self.relation, self.birthdate
            )
    
    SYMBOLS = [k for k in locals().keys() if not k.startswith('_')]
    

def setup_database(DB_URI): 
    '''create a database at the given URI'''
    engine = sa.create_engine(DB_URI) 
    SessionBuilder = sessionmaker(bind=engine) 
    session = SessionBuilder() 
    Base.metadata.create_all(engine) 


def build_database_session(DB_URI): 
    '''create an engine based on the config file'''
    engine = sa.create_engine(DB_URI) 
    SessionBuilder = sessionmaker(bind=engine) 
    session = SessionBuilder() 
    return engine, session 


def object_write(session, entry): 
    '''load an object’s values into the database''' 
    session.add(entry) 

 
def object_bulk_write(session, entry_list): 
    '''load a list of objects into the database'''
    session.add_all(entry_list) 


def query_magic_number(session, number): 
    '''determine whether an entry exists'''
    test1 = session.query(MagicNumber).filter(MagicNumber.magic_number == number).first() 
    return test1 

 
def table_exists(engine, tableName): 
    '''check whether the table exists in the database'''
    insp = sa.inspect(engine) 
    exists = insp.has_table(tableName) 
    return exists
    
    
def insert_or_update(session, entry):
    """upsert"""
    t = entry.__table__
    ins = insert(t)
    edict = {}
    fields = [x for x in entry.SYMBOLS if x != "birthdate"]
    for m in fields:
        edict[m] = getattr(entry, m)
    edict['birthdate'] = hex(edict['magic_number'])
        
    upsert = ins.on_conflict_do_update(constraint=t.primary_key, set_=edict)
    session.execute(upsert, edict)

```  

Now that our data model is defined, let's use it. Instead of reading in the raw values, I've structured the data a bit, by adding two additional fields to each magic number: the person's name, and their relation to Linus. Now simply load the data into the database: [load\_to\_db.py](./load_to_db.py)  

```python
import magic_numbers_model as model
from magic_numbers_model import MagicNumber

 
# connect to database 
DB_URI = 'postgresql://sylvanix:passwd@192.168.2.128:5432/practical'
engine, session = model.build_database_session(DB_URI) 
table_exists = model.table_exists(engine, 'numbers') 
if not table_exists: 
    model.setup_database(DB_URI)

# create a dataset 
first = MagicNumber(672274793, "Linus", "ipse") 
second = MagicNumber(85072278, "Patricia", "daughter") 
third = MagicNumber(369367448, "Daniela", "daughter") 
fourth = MagicNumber(537993216, "Celeste", "daughter") 
mn_list = [first, second, third, fourth] 
 
for mn in mn_list:	
    #exists = model.query_magic_number(session, mn.magic_number) 
    model.insert_or_update(session, mn)

# commit and close session 
session.commit() 
print("Commiting changes ...") 
session.close() 
print("Closing session ...")

```

Check the PGAdmin UI to verify that the data were added.  

![alt text](./images/pgAdmin_ui.png "pgAdmin UI")  


