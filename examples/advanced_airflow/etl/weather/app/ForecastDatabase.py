import os
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert
import psycopg2


Base = declarative_base()


class Forecast(Base):
    """Forecast data model class
    """ 
    __tablename__ = "forecast" 
    
    periodId=sa.Column("periodId", sa.String, nullable=False, primary_key=True)
    number=sa.Column("number", sa.String, nullable=False)
    name=sa.Column("name", sa.String, nullable=False)
    startTime=sa.Column("startTime", sa.DateTime(timezone=True), nullable=False)
    endTime=sa.Column("endTime", sa.DateTime(timezone=True), nullable=False)
    isDaytime=sa.Column("isDaytime", sa.Boolean)
    temperature=sa.Column("temperature", sa.Integer)
    temperatureUnit=sa.Column("temperatureUnit", sa.String)
    
    
    def __init__(self, entry: dict = None): 
        self.Base = Base
        self.periodId = entry.get("period_id", None)
        self.number = entry.get("number", None)
        self.name = entry.get("name", None)
        self.startTime = entry.get("start_time", None)
        self.endTime = entry.get("end_time", None)
        self.isDaytime = entry.get("is_daytime", None)
        self.temperature = entry.get("temperature", None)
        self.temperatureUnit = entry.get("temperature_unit", None)
    
    SYMBOLS = [k for k in locals().keys() if not k.startswith('_')]

    def __repr__(self):
        return "<Forecast( {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} )>"\
                .format(
                    self.periodId, self.number, self.name, self.startTime, self.endTime,
                    self.isDaytime, self.temperature, self.temperatureUnit
                )



class ForecastDB(object): 
    """Forecast database class with methods for connecting, table creation, insertion, etc.
    """ 
    def __init__(self) -> None: 
    
        db_url=os.getenv("DB_URL")
        db_server=os.getenv("DB_SERVER")
        db_table=os.getenv("DB_TABLE")
        db_port=os.getenv("DB_PORT")
        db_user=os.getenv("DB_USER")
        db_password=os.getenv("DB_PASSWORD")

        db_uri = "postgresql://" + db_user + ':' + db_password + \
                 '@' + db_url + ':' + db_port + '/' + db_server
        
        self.tablename = db_table

        # connect to the database 
        self.engine, self.session = self.build_database_session(db_uri)
        if not self.table_exists():
            self.setup_database()
    
    def setup_database(self): 
        '''create a database at the given URI'''
        Base.metadata.create_all(self.engine) 
    
    
    def build_database_session(self, db_uri): 
        '''create an engine based on the config file'''
        engine = sa.create_engine(db_uri) 
        SessionBuilder = sessionmaker(bind=engine) 
        session = SessionBuilder() 
        return engine, session 
    
    
    def object_write(self, closing): 
        '''load an object’s values into the database''' 
        self.session.add(closing) 
    
     
    def object_bulk_write(self, closing_list): 
        '''load a list of objects into the database'''
        self.session.add_all(closing_list) 
    
    
    def query_closing(self, date): 
        '''determine whether an entry exists'''
        test = self.session.query(DJIA).filter(DJIA.date == date).first() 
        return test
    
     
    def table_exists(self): 
        '''check whether the table exists in the database'''
        insp = sa.inspect(self.engine) 
        exists = insp.has_table(self.tablename) 
        return exists


    def fetch_all_entries(self):
        closings = self.session.query(DJIA).all()
        for c in closings:
            print(c)
    

    def upsert(self, entry):
        table = entry.__table__
        stmt = insert(table)
        fields = [c for c in entry.SYMBOLS if c != table.primary_key]
        update_dict = {}
        for c in fields:
             update_dict[c] = getattr(entry, c)
        if not update_dict:
            raise ValueError("insert_or_update resulted in an empty update_dict")
        stmt = stmt.on_conflict_do_update(index_elements=table.primary_key, set_=update_dict)
        self.session.execute(stmt, update_dict)


    def commit_and_close(self):
        self.session.commit()
        print("Commiting changes ...")
        self.session.close()
        print("Closing session ...")
