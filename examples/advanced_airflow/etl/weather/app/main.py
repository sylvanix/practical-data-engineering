import requests
import json

from ForecastDatabase import Forecast, ForecastDB


########################################################################
### Get the weather forecast ###########################################

# to find out the forcast URL
#response = requests.get("https://api.weather.gov/points/34.7305,-86.5861")

response = requests.get("https://api.weather.gov/gridpoints/HUN/63,43/forecast")
r = json.loads(response.text)

########################################################################
### Create a sqlalchemy object and load the database ###################

db = ForecastDB()

updated = r["properties"]["updated"]
periods = r["properties"]["periods"]
for p in periods:
    pdict = {
        "period_id": '_'.join([updated, str(p["number"])]),
        "number": p["number"],
        "name": p["name"],
        "start_time": p["startTime"],
        "end_time": p["endTime"],
        "is_daytime": p["isDaytime"],
        "temperature": p["temperature"],
        "temperature_unit": p["temperatureUnit"],
    }

    db.upsert(Forecast(pdict))

db.commit_and_close()

