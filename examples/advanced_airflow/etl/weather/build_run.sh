#!/bin/bash

source /home/sylvanix/Projects/Propitious/.secrets

IMAGENAME="weather"
TAG="0.0.0"

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$PG_URL \
    --env DB_SERVER=$PG_SERVER \
    --env DB_TABLE=forecast \
    --env DB_PORT=$PG_PORT \
    --env DB_USER=$PG_USER \
    --env DB_PASSWORD=$PG_PASSWORD \
    $IMAGENAME:$TAG
