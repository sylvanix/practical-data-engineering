'''
Example of data model, using SqlAlchemy with a PostgreSQL database 
'''

import sqlalchemy as sa 
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert
import psycopg2 
 

Base = declarative_base() 

 
class MagicNumber(Base):

    __tablename__ = 'numbers'
    magic_number = sa.Column('magic_number', sa.Integer, primary_key=True)
    person = sa.Column('person', sa.String) 
    relation = sa.Column('relation', sa.String) 
    birthdate = sa.Column('birthdate', sa.String)

    def __init__(self, magic_number, person, relation): 
        self.magic_number = magic_number
        self.person = person 
        self.relation = relation
        # convert integer magic_number to hex to reveal birthdate
        self.birthdate = hex(magic_number)
 
    def __repr__(self): 
        return "<MagicNumber({}, {}, {}, {})>".format(
            self.magic_number, self.person, self.relation, self.birthdate
            )
    
    SYMBOLS = [k for k in locals().keys() if not k.startswith('_')]
    

def setup_database(DB_URI): 
    '''create a database at the given URI'''
    engine = sa.create_engine(DB_URI) 
    SessionBuilder = sessionmaker(bind=engine) 
    session = SessionBuilder() 
    Base.metadata.create_all(engine) 


def build_database_session(DB_URI): 
    '''create an engine based on the config file'''
    engine = sa.create_engine(DB_URI) 
    SessionBuilder = sessionmaker(bind=engine) 
    session = SessionBuilder() 
    return engine, session 


def object_write(session, entry): 
    '''load an object’s values into the database''' 
    session.add(entry) 

 
def object_bulk_write(session, entry_list): 
    '''load a list of objects into the database'''
    session.add_all(entry_list) 


def query_magic_number(session, number): 
    '''determine whether an entry exists'''
    test1 = session.query(MagicNumber).filter(MagicNumber.magic_number == number).first() 
    return test1 

 
def table_exists(engine, tableName): 
    '''check whether the table exists in the database'''
    insp = sa.inspect(engine) 
    exists = insp.has_table(tableName) 
    return exists
    
    
def insert_or_update(session, entry):
    """upsert"""
    t = entry.__table__
    ins = insert(t)
    edict = {}
    fields = [x for x in entry.SYMBOLS if x != "birthdate"]
    for m in fields:
        edict[m] = getattr(entry, m)
    edict['birthdate'] = hex(edict['magic_number'])
        
    upsert = ins.on_conflict_do_update(constraint=t.primary_key, set_=edict)
    session.execute(upsert, edict)
