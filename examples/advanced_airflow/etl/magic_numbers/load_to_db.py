import magic_numbers_model as model
from magic_numbers_model import MagicNumber

 
# connect to database 
DB_URI = 'postgresql://meowth:thatsright@192.168.2.129:5432/etls'
engine, session = model.build_database_session(DB_URI) 
table_exists = model.table_exists(engine, 'numbers') 
if not table_exists: 
    model.setup_database(DB_URI)

# create a dataset 
first = MagicNumber(672274793, "Linus", "ipse") 
second = MagicNumber(85072278, "Patricia", "daughter") 
third = MagicNumber(369367448, "Daniela", "daughter") 
fourth = MagicNumber(537993216, "Celeste", "daughter") 
mn_list = [first, second, third, fourth] 
 
for mn in mn_list:	
    #exists = model.query_magic_number(session, mn.magic_number) 
    model.insert_or_update(session, mn)

# commit and close session 
session.commit() 
print("Commiting changes ...") 
session.close() 
print("Closing session ...")
