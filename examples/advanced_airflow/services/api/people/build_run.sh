#!/bin/bash

IMAGENAME="person-api"
TAG="0.0.0"
CONTAINERNAME="random-person-api"

docker build -t $IMAGENAME:$TAG .

docker run -d -p 80:80 --name $CONTAINERNAME $IMAGENAME:$TAG
