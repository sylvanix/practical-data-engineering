from fastapi import FastAPI
import tasks

app = FastAPI()


@app.get("/")
def read_root():
    return {"Random Person API:": "returns a randomly-generated person's information"}


@app.get("/person")
def read_person():
    return tasks.generate_person()
