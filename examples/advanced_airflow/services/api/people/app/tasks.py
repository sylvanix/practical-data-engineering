import random
from datetime import datetime
from faker import Faker
from faker.providers import BaseProvider


def generate_person():
    fake = Faker()
    
    countries = {
        "Great Britain": {'locale': 'en_GB', 'w': 23},
        "Germany": {'locale': 'de_DE', 'w': 21},
        "Denmark": {'locale': 'da_DK', 'w': 9},
        "Norway": {'locale': 'no_NO', 'w': 11},
        "Sweden": {'locale': 'sv_SE', 'w': 15},
        "Finland": {'locale': 'fi_FI', 'w': 4},
        "Japan": {'locale': 'jp_JP', 'w': 25},
        "South Korea": {'locale': 'ko_KR', 'w': 17},
        "Australia": {'locale': 'en_AU', 'w': 8},
        "United States": {'locale': 'en_US', 'w': 39}
    }
    
    weights = [x['w'] for x in list(countries.values())]

    class CountryProvider(BaseProvider):
        def country(self):
            return random.choices(list(countries.keys()), weights=weights, k=1)[0]
        
    fake.add_provider(CountryProvider)
    country = fake.country()
    locale = countries[country]['locale']
    fake = Faker(locale)
    name = fake.name()
    address = fake.address()
    dob = fake.date_of_birth()

    return {"name": name, "date_of_birth": dob, "address": address, "country": country}
