from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.operators.bash import BashOperator
 
from datetime import datetime
 
with DAG(
    dag_id='forecast',
    start_date=datetime(2024, 4, 19),
    schedule_interval=None,
    tags=["BashOperator"]
) as dag:
 
    t_start = EmptyOperator(
        task_id='start'
    )
 
    t_forecast = BashOperator(
        task_id='get_forecast',
        bash_command="curl -X GET \
                        https://api.weather.gov/gridpoints/HUN/63,43/forecast \
                        -H 'accept: application/json' \
                        > /Propitious/weather.txt 2> err.txt \
                     "
    )
 
    t_end = EmptyOperator(
        task_id='end'
    )
 
t_start >> t_forecast
t_forecast >> t_end
