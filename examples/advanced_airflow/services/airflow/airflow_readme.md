## Install and Test Apache Airflow  

Move into the airflow\_stack directory.  

```bash
cd services_stack
```

There are four files in the dir needed to bring up airflow: .env, airflow.cfg, docker-compose.airflow.yml, and services-stack  

First, let's look at the .env file:  

```bash
# AIRFLOW POSTGRES
POSTGRES_USER=airflow
POSTGRES_PASSWORD=airflow
POSTGRES_DB=airflow
 
# Airflow Core
AIRFLOW__CORE__FERNET_KEY=AHxWWgdbsP6DPYt_JEDjAGiQyU4PRCKqwzWJQ3Wteow=
AIRFLOW__CORE__EXECUTOR=LocalExecutor
AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION=True
AIRFLOW__CORE__LOAD_EXAMPLES=False
AIRFLOW_UID=0
 
# Backend DB
AIRFLOW__DATABASE__SQL_ALCHEMY_CONN=postgresql+psycopg2://airflow:airflow@postgres/airflow
AIRFLOW__DATABASE__LOAD_DEFAULT_CONNECTIONS=False
 
# Airflow Init
_AIRFLOW_DB_UPGRADE=True
_AIRFLOW_WWW_USER_CREATE=True
_AIRFLOW_WWW_USER_USERNAME=airflow
_AIRFLOW_WWW_USER_PASSWORD=airflow
```

You can create your own Fernet key for the "AIRFLOW\_\_CORE\_\_FERNET" line above using the Cryptography module in Python. I activate my virtual Python 3.11 environment, then:  

```bash
pip install cryptography

python
>>> from cryptography.fernet import Fernet
>>> fernet_key = Fernet.generate_key()
>>> print(fernet_key.decode())

```

The airflow.cfg file:  

```bash
[core]
 The home directory for Airflow
airflow_home = $PWD/airflow

# The executor class that Airflow should use. Choices include SequentialExecutor, LocalExecutor, and CeleryExecutor.
executor = LocalExecutor

[webserver]
# The base URL for your Airflow web server
base_url = http://localhost:8080

# The IP address to bind to
web_server_host = 0.0.0.0

# The port to bind to
web_server_port = 8080
```

The docker-compose.airflow.yml file:  

```bash
version: '3.4'

x-common:
  &common
  image: apache/airflow:2.7.1-python3.11
  user: "${AIRFLOW_UID}:0"
  env_file: 
    - $PWD/.env
  volumes:
    - $PWD/airflow/dags:/opt/airflow/dags
    - $PWD/airflow/logs:/opt/airflow/logs
    - $PWD/airflow/plugins:/opt/airflow/plugins
    - /var/run/docker.sock:/var/run/docker.sock

x-depends-on:
  &depends-on
  depends_on:
    postgres:
      condition: service_healthy
    airflow-init:
      condition: service_completed_successfully

services:
  postgres:
    image: postgres:15.4-bookworm
    container_name: airflow-postgres
    ports:
      - "5434:5432"
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "airflow"]
      interval: 5s
      retries: 5
    env_file:
      - $PWD/.env
    environment:
      - POSTGRES_USER=$POSTGRES_USER
      - POSTGRES_PASSWORD=$POSTGRES_PASSWORD
      - POSTGRES_DB=$POSTGRES_DB
    volumes:
      - $PWD/volumes/airflow/postgres:/var/lib/postgresql/data

  scheduler:
    <<: *common
    <<: *depends-on
    container_name: airflow-scheduler
    command: scheduler
    restart: on-failure
    ports:
      - "8793:8793"

  webserver:
    <<: *common
    <<: *depends-on
    container_name: airflow-webserver
    restart: always
    command: webserver
    ports:
      - "8080:8080"
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8080/health"]
      interval: 30s
      timeout: 30s
      retries: 5
  
  airflow-init:
    <<: *common
    container_name: airflow-init
    entrypoint: /bin/bash
    command:
      - -c
      - exec /entrypoint airflow version
```

The Bash script "services-stack" that starts/stops Airflow and the other services on which it depends:  

```bash
#!/bin/bash

# define variables
NETWORK_NAME=test-net

# create the network if it hasn't been already
NETWORK_CREATED=$(docker network ls | grep $NETWORK_NAME | wc -l)

[[ "$NETWORK_CREATED" == "1" ]] || docker network create $NETWORK_NAME


# create the following directories to prevent root from owning them
[[ "$1" == "up" ]] \
    && mkdir $PWD/airflow \
	&& mkdir $PWD/airflow/dags \
    && mkdir $PWD/airflow/logs \
    && mkdir $PWD/airflow/plugins \
    && mkdir $PWD/volumes \
    && mkdir -p $PWD/volumes/airflow/postgres


# bring up/down the services defined in the docker-compose files
docker-compose -f $PWD/docker-compose.airflow.yml \
               $@
```

We're not using the 3 Docker "NETWORK" entries in the file above, but may as well leave them there for when we need it at some later time.  

If you haven't already started the databases stack, then do it now. Bring up Airflow with:  

```bash
./services-stack up -d
```

After a minute or two, the service should be available. Access the UI here: http://localhost:8080  

Look at the DAGs tab. I placed two DAGs in the airflow\_stack/airflow/dags directory. You should see  

![alt text](../images/dags_window.png "Airflow DAGs")  

Since your Airflow install has not yet been run, you won't see any numbers in the Runs column. Try running the "hello\_test" DAG now by activating it with the slider switch button in the leftmost column. Then click on the "play" triangle button under "Actions". If you want to understand the DAG, then you can view the code in your text editor, remember that it's in the "dags" directory, or you can click on the name of the DAG, in this case "hello\_test", and then clicking on "Code".  

Now try running the second DAG, "water\_and\_stocks". Here we have combined the two database examples that we have run already into a single DAG.  

Assuming that it ran on your system, click on the name of the DAG and view the "details" tab:  

![alt text](../images/water_stocks_details.png "water_and_stocks_dag")  

In the "Details" tab, as above, individual runs are represented by the colored lines to the left. They are dark green after running successfully.  The length of the top line represents the duration of the run. The bottom two squares represent the separate tasks "water\_purity\_etl" and djia\_etl. Click on the Code tab to see the Python Dag code (or go to the dag directory and view with your editor):  

```Python
import os
import subprocess
import datetime
import pendulum
from copy import deepcopy

from airflow import DAG
from airflow.hooks.base_hook import BaseHook
from airflow.providers.docker.operators.docker import DockerOperator
from docker.types import Mount

args = {
    'owner': 'your-username',
    'retries': 1,
    'retry-delay': datetime.timedelta(minutes=3)
}

dag = DAG(
    dag_id = "water_and_stocks",
    default_args = args,
    start_date = pendulum.datetime(2023, 9, 28, 9, 0, tz="America/Chicago"),
    schedule_interval = '0 9 * * *',
    catchup = False
)

adb_environment = {
    "DB_URL": "http://localhost:1234",
    "DB_USER": "meowth",
    "DB_PASSWORD": "thatsright",
    "DB_NAME": "water",
    "DATA_DIR": "/data"
}

#adb_environment = {
#    "DB_URL": BaseHook.get_connection("adb_connection").host,
#    "DB_USER": BaseHook.get_connection("adb_connection").login,
#    "DB_PASSWORD": BaseHook.get_connection("adb_connection").password,
#    "DB_NAME": BaseHook.get_connection("adb_connection").schema,
#    "DATA_DIR": "/data"
#}

pg_environment = {
    "DB_URL": "127.0.0.1",
    "DB_SERVER": "practical",
    "DB_PORT": 5432,
    "DB_USER": "meowth",
    "DB_PASSWORD": "thatsright",
    "DB_TABLE": "djia",
    "DATA_DIR": "/data"
}

#pg_environment = {
#    "DB_URL": BaseHook.get_connection("pg_connection").host,
#    "DB_SERVER": BaseHook.get_connection("pg_connection").schema,
#    "DB_PORT": BaseHook.get_connection("pg_connection").port,
#    "DB_USER": BaseHook.get_connection("pg_connection").login,
#    "DB_PASSWORD": BaseHook.get_connection("pg_connection").password,
#    "DB_TABLE": "djia",
#    "DATA_DIR": "/data"
#}

data_mount = Mount(
    source="/home/sylvanix/data",
    target="/data",
    type="bind"
)

t_water = DockerOperator(
    task_id="water_purity_etl",
    dag=dag,
    api_version='auto',
    auto_remove=True,
    image='water_quality_etl:0.0.1',
    container_name='water_etl',
    docker_url="unix://var/run/docker.sock",
    network_mode="host",
    mount_tmp_dir=False,
    environment=adb_environment,
    mounts=[data_mount]
)

t_stocks = DockerOperator(
    task_id="djia_etl",
    dag=dag,
    api_version='auto',
    auto_remove=True,
    image='stocks_etl:0.0.1',
    container_name='stocks_etl',
    docker_url='unix://var/run/docker.sock',
    network_mode="host",
    mount_tmp_dir=False,
    environment=pg_environment,
    mounts=[data_mount]
)


t_water >> t_stocks
```

Notice that the code sections "adb\_environment" and "pg\_environment" each have a version which is commented out. The un-commented environment is just to get you going, using a test database with test users. When you are ready to "move into production", be sure to replace these uncommented enviroment sections, which contain sensitive information such as username and password with their commented counterparts which use Airflow Connections via the BaseHook method. You can set up your own Airflow connections from the Airflow UI by clicking "Admin" in the top menu, and then "Connections". The two connections configured for this example are shown below:  

![alt text](../images/connections.png "Airflow Connections")  
