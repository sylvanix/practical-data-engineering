from confluent_kafka import Consumer

def my_commit_callback():
    print("callback ...")


# consumer instance
conf = {
        'bootstrap.servers':'localhost:9092',
        'group.id':'python-consumer',
        'auto.offset.reset':'earliest',
        'fetch.wait.max.ms': 500
        }
c = Consumer(conf)
print('Kafka Consumer has been initiated...')


# determine the name of the topic, and then subscribe to it
print('Available topics to consume: ', c.list_topics().topics)
c.subscribe(['user-tracker'])


# poll the topic, looking for available messages
while True:
    msg=c.poll(1.0) #timeout
    if msg is None:
        continue
    if msg.error():
        print('Error: {}'.format(msg.error()))
        continue
    data=msg.value().decode('utf-8')
    print(data)





c.close()
