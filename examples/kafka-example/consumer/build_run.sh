#!/bin/bash

IMAGENAME="pkafka"
TAG="consumer"
CONTAINERNAME="kafka-consumer"

HOSTDATA=/home/sylvanix/data

docker build --quiet -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    $IMAGENAME:$TAG
