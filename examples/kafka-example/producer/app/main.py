import json
import time
import logging
import random
from faker import Faker
from confluent_kafka import Producer


fake=Faker()


# configure the logger format
logging.basicConfig(format='%(asctime)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='/data/producer.log',
                    filemode='w')

logger = logging.getLogger()
logger.setLevel(logging.INFO)


# create the producer by specifying the port of the Kafka cluster
p=Producer({'bootstrap.servers':'localhost:9092'})
print('Kafka Producer has been initiated...')


# callback function to acknowledge new sign-ups or errors
def receipt(err,msg):
    if err is not None:
        print('Error: {}'.format(err))
    else:
        message = 'Produced message on topic {} with value of {}\n'.format(
            msg.topic(), msg.value().decode('utf-8')
        )
        logger.info(message)
        print(message)


# a loop that simulates user sign-up events
for i in range(1000):
    data={
       'user_id': fake.random_int(min=20000, max=100000),
       'user_name':fake.name(),
       'user_address':fake.street_address() + ' | ' + fake.city() \
            + ' | ' + fake.country_code(),
       'platform': random.choice(['Mobile', 'Laptop', 'Tablet']),
       'signup_at': str(fake.date_time_this_month())    
       }
    m=json.dumps(data)
    p.poll(1)
    p.produce('user-tracker', m.encode('utf-8'),callback=receipt)
    p.flush()
    time.sleep(2)
