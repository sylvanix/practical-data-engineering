#!/bin/bash

IMAGENAME="pkafka"
TAG="producer"
CONTAINERNAME="kafka-producer"

HOSTDATA=/home/sylvanix/data

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    $IMAGENAME:$TAG
