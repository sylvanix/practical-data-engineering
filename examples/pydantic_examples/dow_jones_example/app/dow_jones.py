from typing import Optional

from pydantic import BaseModel


class DJIA(BaseModel):
    """Dow Jones Industrial Average data model class
    """
    Date: str
    MMM: float | None
    American_Express: float | None
    Boeing: float | None
    Caterpillar: float | None
    Coca_Cola: float | None
    IBM: float | None
    Intel: float | None
    J_and_J: float | None
    JPMorgan: float | None
    McDonalds: float | None
    Merck_and_Co: float | None
    Microsoft: float | None
    Procter_and_Gamble: float | None
    The_Home_Depot: float | None
    Walmart: float | None
    Walt_Disney: float | None
    DJIA: Optional[float] = None

def convert_to_DJIA(data: dict) -> dict:
    djia = DJIA(**data).model_dump()
    djia["_key"] = data["Date"].replace('/', '-')
    return djia 
