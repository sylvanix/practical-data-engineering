"""
An ETL (Extract, Transform, Load) example which reads in data from a source file, cleans it, and loads it in a database.
"""

import os
from pathlib import Path

import pandas as pd

from dow_jones import convert_to_DJIA
from database_manager import DBManager


# Get the environment variables made available to the container
db_url = os.getenv("DB_URL")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")

# Connect to the database collection
dowjones = DBManager(
    db_url,
    db_name,
    db_user,
    db_password,
    collection_name="djia"
).collection


# --- EXTRACT ---
filepath = Path(data_dir) / "Dow_Jones_Historical_Data.csv"
df = pd.read_csv(filepath)
header = [x.rstrip() for x in list(df.columns)]


# --- TRANSFORM ---
df = df.where(df.notnull(), '')
df = df.replace('?', '')


# --- LOAD ---
dfstring = df.to_string(
    header=True,
    index=False,
    index_names=False
).split('\n')
rows = [ele.split() for ele in dfstring]
rows = rows[1:]

# rename some dict keys to match our data model in dow_jones.py
header[header.index("J&J")] = "J_and_J"
header[header.index("Procter & Gamble")] = "Procter_and_Gamble"
header[header.index("Merck & Co.")] = "Merck_and_Co"

# in header replace spaces with underscores
header = [x.replace(' ', '_') for x in header]

# load the rows into the Arango collection
for row in rows:
    rdict = dict(zip(header, row))
    djia = convert_to_DJIA(rdict)
    dowjones.insert(djia, overwrite=True)
