from arango import ArangoClient

class DBManager():

    def __init__(self, db_url, db_name, db_user, db_password, collection_name):

        # Initialize the client for ArangoDB.
        client = ArangoClient(hosts=db_url)
        
        # Connect to the "_system" database as root user.
        sys_db = client.db("_system", username=db_user, password=db_password)
        
        # Create the database if it does not exist
        if not sys_db.has_database(db_name):
            sys_db.create_database(db_name)
        
        # Connect to the database as root user.
        db = client.db(db_name, username=db_user, password=db_password)
        
        # Create the collection if it does not exist
        if db.has_collection(collection_name):
            self.collection = db.collection(collection_name)
        else:
            self.collection = db.create_collection(
                name = collection_name,
                #user_keys = False,
                #key_generator = "autoincrement",
                #key_offset = 1,
                #key_increment = 1,
            )
