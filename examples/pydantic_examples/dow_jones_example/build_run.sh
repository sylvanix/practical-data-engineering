#!/bin/bash

source /home/sylvanix/.secrets

HOSTDATA="/media/sylvanix/data1/datasets"
IMAGENAME="stocks_etl"
TAG="0.0.1"

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$ADB_URL \
    --env DB_NAME=stocks \
    --env DB_USER=$ADB_USER \
    --env DB_PASSWORD=$ADB_PASSWORD \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
