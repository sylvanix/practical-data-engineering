"""
An ETL (Extract, Transform, Load) example which reads in data from a source file, cleans it, and loads it in a database.
"""

import os
from pathlib import Path

import pandas as pd

from database_manager import DBManager
from water_quality import convert_to_WaterQuality


# Get the environment variables made available to the container
db_url = os.getenv("DB_URL")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")


#####################################################
# DATABASE CREATION & CONNECTION
####################################################
potability = DBManager(
    db_url,
    db_name,
    db_user,
    db_password,
    collection_name="potability"
).collection

#####################################################
# ETL
####################################################
# (EXTRACT) Read the data from the csv
data_path = Path(data_dir) / "water_potability.csv"
df = pd.read_csv(data_path)

# (TRANSFORM) Handle the missing values
null_cols = ['ph','Sulfate','Trihalomethanes']
mask1 = df['Potability']==0
mask2 = df['Potability']==1
for col in null_cols:
    if col!='Sulfate':
        df[col].fillna(df[col].mean(), inplace=True)
    else:
        df.loc[mask1,col] = df.loc[mask1,col].fillna(df.loc[mask1, col].mean())
        df.loc[mask2,col] = df.loc[mask2,col].fillna(df.loc[mask2, col].mean())

# (LOAD) Write each record (row) to the database collection
dfstring = df.to_string(header=True,
                  index=False,
                  index_names=False).split('\n')
rows = [ele.split() for ele in dfstring]
header = rows[0]
rows = rows[1:]
for row in rows:
    rdict = dict(zip(header, row))
    # instantiate an instance of the Pydantic data class WaterQuality
    wq = convert_to_WaterQuality(rdict)
    # insert into the ArangoDB collection
    potability.insert(wq)
