import os
import subprocess
import datetime
import pendulum
from copy import deepcopy

from airflow import DAG
from airflow.hooks.base_hook import BaseHook
from airflow.providers.docker.operators.docker import DockerOperator
from docker.types import Mount

args = {
    'owner': 'sylvanix',
    'retries': 1,
    'retry-delay': datetime.timedelta(minutes=3)
}

dag = DAG(
    dag_id = "water_and_stocks",
    default_args = args,
    start_date = pendulum.datetime(2023, 9, 28, 9, 0, tz="America/Chicago"),
    schedule_interval = '0 9 * * *',
    catchup = False
)

adb_environment = {
    "DB_URL": "http://localhost:1234",
    "DB_USER": "meowth",
    "DB_PASSWORD": "thatsright",
    "DB_NAME": "water",
    "DATA_DIR": "/data"
}

#adb_environment = {
#    "DB_URL": BaseHook.get_connection("adb_connection").host,
#    "DB_USER": BaseHook.get_connection("adb_connection").login,
#    "DB_PASSWORD": BaseHook.get_connection("adb_connection").password,
#    "DB_NAME": BaseHook.get_connection("adb_connection").schema,
#    "DATA_DIR": "/data"
#}

pg_environment = {
    "DB_URL": "127.0.0.1",
    "DB_SERVER": "practical",
    "DB_PORT": 5432,
    "DB_USER": "meowth",
    "DB_PASSWORD": "thatsright",
    "DB_TABLE": "djia",
    "DATA_DIR": "/data"
}

#pg_environment = {
#    "DB_URL": BaseHook.get_connection("pg_connection").host,
#    "DB_SERVER": BaseHook.get_connection("pg_connection").schema,
#    "DB_PORT": BaseHook.get_connection("pg_connection").port,
#    "DB_USER": BaseHook.get_connection("pg_connection").login,
#    "DB_PASSWORD": BaseHook.get_connection("pg_connection").password,
#    "DB_TABLE": "djia",
#    "DATA_DIR": "/data"
#}

data_mount = Mount(
    source="/home/sylvanix/data",
    target="/data",
    type="bind"
)

t_water = DockerOperator(
    task_id="water_purity_etl",
    dag=dag,
    api_version='auto',
    auto_remove=True,
    image='water_quality_etl:0.0.1',
    container_name='water_etl',
    docker_url="unix://var/run/docker.sock",
    network_mode="host",
    mount_tmp_dir=False,
    environment=adb_environment,
    mounts=[data_mount]
)

t_stocks = DockerOperator(
    task_id="djia_etl",
    dag=dag,
    api_version='auto',
    auto_remove=True,
    image='stocks_etl:0.0.1',
    container_name='stocks_etl',
    docker_url='unix://var/run/docker.sock',
    network_mode="host",
    mount_tmp_dir=False,
    environment=pg_environment,
    mounts=[data_mount]
)


t_water >> t_stocks
