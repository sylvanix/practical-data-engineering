from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.operators.bash import BashOperator
 
from datetime import datetime
 
with DAG(
    dag_id='hello_test',
    start_date=datetime(2023, 9, 15),
    schedule_interval=None
) as dag:
 
    t_start = EmptyOperator(
        task_id='start'
    )
 
    t_print = BashOperator(
        task_id='print_hello',
        bash_command='echo "Hello Airflow"'
    )
 
    t_end = EmptyOperator(
        task_id='end'
    )
 
t_start >> t_print
t_print >> t_end
