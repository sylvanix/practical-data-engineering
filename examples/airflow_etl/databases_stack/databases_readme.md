## Install and Test PostgreSQL & PGAdmin4

### A containerized PostgreSQL ETL example  


Let's look at how to model a tiny dataset with the Python library SQLAlchemy and load it into a PostgreSQL database which we can inspect in a browser UI.  


#### Setting up PostgreSQL  
Create a project directory with volume directories for Postgres and Pgadmin. The pgadmin volume dir needs to be owned by uid = 5050 and in the group with gid = 5050 in order for its files to be written and accessed there.  

```    
$ mkdir volumes  
$ cd volumes  
$ mkdir postgres pgadmin
$ sudo chown 5050 pgadmin && sudo chgrp 5050 pgadmin
$ cd ..  

```  

Let's look at the docker-compose file [docker-compose-postgres.yml](./docker-compose-postgres.yml):     


```docker-compose
version: '3.7'

services:

  db:
    container_name: postgres_db_stack
    image: postgres:15.4-bookworm
    restart: unless-stopped
    ports:
      - "5432:5432"
    volumes:
       - ./volumes/postgres:/var/lib/postgresql/data
    secrets:
      - db_secret
    networks:
      - test-net

  pgadmin:
    container_name: pgadmin_db_stack
    image: dpage/pgadmin4:7.4
    restart: unless-stopped
    ports:
      - "5050:80"
    volumes:
       - ./volumes/pgadmin:/var/lib/pgadmin
    secrets:
      - db_secret
    networks:
      - test-net

secrets:
  db_secret:
    file: ./.secrets

networks:
  test-net:
    external:
      name: test-net
```  

Notice the reference in the file above to a file named ".secrets". Let's look at it:  

```
POSTGRES_USER=meowth
POSTGRES_PASSWORD=thatsright
POSTGRES_DB=practical

PGADMIN_DEFAULT_EMAIL=meowth@rocket.org
PGADMIN_DEFAULT_PASSWORD=thatsright

ARANGO_USER=root
ARANGO_ROOT_PASSWORD=reallysecurepassword
```


##### Pull the images and run the containers:  

```bash 
$ docker-compose -f docker-compose-postgres.yml up  -d
```

After the images are pulled, and the containers built, you should see the following:  

```Bash
Creating postgres_container ... done
Creating pgadmin_container  ... done
```  


Check the status with ```docker ps```  

```bash
$ docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED          STATUS          PORTS                                            NAMES
e5fe3cc241fb   dpage/pgadmin4    "/entrypoint.sh"         52 seconds ago   Up 50 seconds   443/tcp, 0.0.0.0:5050->80/tcp, :::5050->80/tcp   pgadmin_container
78f72e72195c   postgres:latest   "docker-entrypoint.s…"   52 seconds ago   Up 50 seconds   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp        postgres_container
```  

Now you can go to your browser and sign into the PGAdmin4 UI at **localhost:5050** using the PGADMIN default email and password from the .env file.  


Before we can create a database table and load data to it, well need to create a database named "practical" (just what I called it in this example). Our containerized ETL below, finding that no table exist in this database, will create one.  

......


#### Containerized ETL (PostgreSQL)  

Back in the root directory of this project, CD to the directory docker project "dow\_jones\_example". The Dockerfile specifies our project:  

```Bash
# specify a base image
FROM python:3.11.5-bookworm

USER root

#define the user - get UID & GID from the id command
ARG USERNAME=your_username
ARG USERID=1000
ARG GNAME="your_group"
ARG GID=1000


# install pip and create the user
RUN apt-get update && apt-get install -y --no-install-recommends \
                                      python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd -g $GID $GNAME \
    && useradd -ms /bin/bash --uid $USERID --gid $GID \
               --groups $GNAME $USERNAME


# make the /data dir in the container (this can be a mapped host dir in the docker run script)
RUN mkdir /data

# copy files to the container
WORKDIR /workspace
COPY ./requirements.txt /workspace

USER $USERNAME

# install Python modules
RUN pip3 install --upgrade pip \
    && pip3 install --no-cache-dir -r /workspace/requirements.txt

# copy the python files to the container
COPY ./app/ /workspace


# entrypoint - run the app
CMD ["python3", "main.py"]
```

The RUN commands are executed as root, until the USER command is reached and run.  

To see your USERID and GID, run following in your command terminal:  

```Bash
id
```

On my Linux system, this shows that I'm user 1000 and in group 1000.  

At the end of the Dockerfile, the Python application in the "app" subdirectory is run.  

The Bash script, shown below, "buld\_run.sh" builds (unless already built) and runs the Dockerfile.  

```Bash
#!/bin/bash

source /home/my_user_name/.secrets

HOSTDATA="/home/my_user_name/data"
IMAGENAME="stocks_etl"
TAG="0.0.1"

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$PG_URL \
    --env DB_SERVER=$PG_SERVER \
    --env DB_TABLE=djia \
    --env DB_PORT=$PG_PORT \
    --env DB_USER=$PG_USER \
    --env DB_PASSWORD=$PG_PASSWORD \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
```

In the file above, we mount the volume HOSTDATA so that the container can read the datafile found there. You will need to place the file "Dow\_Jones\_Historical\_Data.csv" (included with the files here) in the HOSTDATA directory you specified in the "build_run.sh" script.  

The environment variables defined in the script are made available through the line at the top of the file  

 ```source /home/my_user_name/.secrets```  
 
 which reads the hidden .secrets file. We are following the practice that no username, password, or other connection data should be in a file or script which could then be committed to a code repository and seen by others. Make sure that you exclude the .secrets file from your Git repository by using a .gitignore file. Here is the .secrets file used in this example:  

```Bash
#!/bin/bash

# Postgres Database
PG_URL=127.0.0.1
PG_SERVER=practical
PG_PORT=5432
PG_USER=meowth
PG_PASSWORD=thatsright

# ArangoDB Database
ADB_URL='http://localhost:1234'
ADB_USER=meowth
ADB_PASSWORD=thatsright
```

We are only concerned with the Postgres section for now.  

Build and run the containerized app:  

```Bash
bash build_run.sh
```

You should see output similar to this:  

```
dow_jones_example$ bash build_run.sh 
[+] Building 52.2s (13/13) FINISHED                                                      docker:default
 => [internal] load .dockerignore                                                                  0.0s
 => => transferring context: 2B                                                                    0.0s
 => [internal] load build definition from Dockerfile                                               0.0s
 => => transferring dockerfile: 1.06kB                                                             0.0s
 => [internal] load metadata for docker.io/library/python:3.11.5-bookworm                          0.5s
 => [auth] library/python:pull token for registry-1.docker.io                                      0.0s
 => [internal] load build context                                                                  0.0s
 => => transferring context: 6.19kB                                                                0.0s
 => CACHED [1/7] FROM docker.io/library/python:3.11.5-bookworm@sha256:844b3044eef9d990d3c640e0462  0.0s
 => [2/7] RUN apt-get update && apt-get install -y --no-install-recommends                        13.8s
 => [3/7] RUN mkdir /data                                                                          0.4s
 => [4/7] WORKDIR /workspace                                                                       0.0s 
 => [5/7] COPY ./requirements.txt /workspace                                                       0.0s 
 => [6/7] RUN pip3 install --upgrade pip     && pip3 install --no-cache-dir -r /workspace/requir  35.2s 
 => [7/7] COPY ./app/ /workspace                                                                   0.0s 
 => exporting to image                                                                             2.2s 
 => => exporting layers                                                                            2.2s
 => => writing image sha256:52537bbd266ba0cd1f8dfd1002f83a2730598bdb059827e2dabb0b6cf476f365       0.0s 
 => => naming to docker.io/library/stocks_etl:0.0.1                                                0.0s 
Commiting changes ...                                                                                   
Closing session ...

```

Check the PGAdmin UI (localhost:5050) to see the newly created table named "djia" under the database named "practical". Admittedly, learning your way around the PGAdmin UI can be frustrating at first, but it will be worth it when you have several databases, each with multiple tables; and it sure beats having to use the command line tool psql.  

![alt text](../images/djia_table.png "DJIA Table")  

---

### An ArangoDB containerized ETL example 


#### Setting up ArangoDB  

Let's look at the docker-compose file [docker-compose-arangodb.yml](./docker-compose-arangodb.yml):     


```docker-compose
version: '3.7'

services:
  arangodb:
    image: arangodb:3.10
    container_name: "arangodb"
    restart: unless-stopped
    ports:
      - 1234:8529
    networks:
      - test-net
    #environment:
    #  ARANGO_USER: root
    #  ARANGO_ROOT_PASSWORD: reallysecurepassword
    volumes:
      - $PWD/volumes/arango-etl:/var/lib/arangodb3
    secrets:
      - db_secret     

secrets:
  db_secret:
    file: ./.secrets

networks:
  test-net:
    external:
      name: test-net
```

Notice that the environment variables are commented-out. We are reading them instead from the file ".secrets", shown below:  

```
POSTGRES_USER=meowth
POSTGRES_PASSWORD=thatsright
POSTGRES_DB=practical

PGADMIN_DEFAULT_EMAIL=meowth@rocket.org
PGADMIN_DEFAULT_PASSWORD=thatsright

ARANGO_USER=root
ARANGO_ROOT_PASSWORD=reallysecurepassword
```

Build and run ArangoDB:  

```Bash
bash docker-compose -f docker-compose-arangodb.yml up -d
```

The ArangoDB UI can be accessed from localhost:1234:  

![alt text](../images/arango_ui.png "ArangoDB UI")  

With the database service running, let's look at the ETL code and run it.  

The Dockerfile looks the same as the one for the Postgres ETL example above, but the other files are different. The build_run.sh file uses a similar structure to the previous one, but the environment variables are for connecting to an ArangoDB database.  

```Bash
#!/bin/bash

source /home/sylvanix/.secrets

# build
IMAGENAME="water_quality_etl"
TAG="0.0.1"
docker build -t $IMAGENAME:$TAG .

# run
HOSTDATA="/home/your_user_name/data"
docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$ADB_URL \
    --env DB_NAME=water \
    --env DB_USER=$ADB_USER \
    --env DB_PASSWORD=$ADB_PASSWORD \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
```

This Bash script sources my .secrets file, as in the Postgres example, but it could also source the local .secrets file used by the docker-compose-arangodb.yml file.  

You can always run the app using Root DB\_NAME and DB\_PASSWORD, but to run as another user, you must, through the ArangoDB UI, add the new user and edit the collection so that this user had permission to read, write, and edit.  

The Python app looks in the HOSTDATA directory for a file named ""water_potability.csv". So, put that file (included with the files here) in that directory.  

Now build and run the app:

```Bash
bash build_run.sh
```

You should get output similar to the following:  

```
water_quality_example$ bash build_run.sh 
[+] Building 0.5s (13/13) FINISHED                                                       docker:default
 => [internal] load .dockerignore                                                                  0.0s
 => => transferring context: 2B                                                                    0.0s
 => [internal] load build definition from Dockerfile                                               0.0s
 => => transferring dockerfile: 1.03kB                                                             0.0s
 => [internal] load metadata for docker.io/library/python:3.11.5-bookworm                          0.4s
 => [auth] library/python:pull token for registry-1.docker.io                                      0.0s
 => [internal] load build context                                                                  0.0s
 => => transferring context: 91B                                                                   0.0s
 => [1/7] FROM docker.io/library/python:3.11.5-bookworm@sha256:844b3044eef9d990d3c640e046241ac396  0.0s
 => CACHED [2/7] RUN apt-get update && apt-get install -y --no-install-recommends                  0.0s
 => CACHED [3/7] RUN mkdir /data                                                                   0.0s
 => CACHED [4/7] WORKDIR /workspace                                                                0.0s
 => CACHED [5/7] COPY ./requirements.txt /workspace                                                0.0s
 => CACHED [6/7] RUN pip3 install --upgrade pip     && pip3 install --no-cache-dir -r /workspace/  0.0s
 => CACHED [7/7] COPY ./app/ /workspace                                                            0.0s
 => exporting to image                                                                             0.0s
 => => exporting layers                                                                            0.0s
 => => writing image sha256:3bfdba56735f04fd5de5ebfcdd0e7be55dda9c0f7fa506d3a8e0b084bc2a3003       0.0s
 => => naming to docker.io/library/water_quality_etl:0.0.1
```

