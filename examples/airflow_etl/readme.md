# Running Python ETLs with Airflow  

### Development environment  

The following examples assume that you have a Linux system with Docker installed, and that you'll be running the examples as a user that is a member of the docker group.  


### Databases  

Before we can use Airflow to run our scheduled ETLs, we must install and configure our SQL and NoSQL databases. Click ["HERE"](./databases\_stack/databases\_readme.md) for a walk-through on installing those services and testing with containerized ETLs.  

### Airflow  

Now we're ready for Airflow. Click ["HERE"](./airflow\_stack/airflow\_readme.md) for a walk-through on installation and testing.  