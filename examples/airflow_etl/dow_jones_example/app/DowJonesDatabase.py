import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert
import psycopg2


Base = declarative_base()


class DJIA(Base):
    """Dow Jones Industrial Average data model class
    """ 
    __tablename__ = "djia" 
    
    date=sa.Column("date", sa.String, nullable=False, primary_key=True) 
    mmm=sa.Column("mmm", sa.Float, nullable=True) 
    amex=sa.Column("amex", sa.Float, nullable=True) 
    boeing=sa.Column("boeing", sa.Float, nullable=True) 
    caterpillar=sa.Column("caterpillar", sa.Float, nullable=True) 
    cocacola=sa.Column("cocacola", sa.Float, nullable=True) 
    ibm=sa.Column("ibm", sa.Float, nullable=True) 
    intel=sa.Column("intel", sa.Float, nullable=True) 
    jj=sa.Column("jj", sa.Float, nullable=True) 
    jpmorgan=sa.Column("jpmorgan", sa.Float, nullable=True) 
    mcdonalds=sa.Column("mcdonalds", sa.Float, nullable=True) 
    merck=sa.Column("merck", sa.Float, nullable=True) 
    microjunk=sa.Column("microjunk", sa.Float, nullable=True) 
    procterandgamble=sa.Column("procterandgamble", sa.Float, nullable=True) 
    homedepot=sa.Column("homedepot", sa.Float, nullable=True) 
    walmart=sa.Column("walmart", sa.Float, nullable=True) 
    waltdisney=sa.Column("waltdisney", sa.Float, nullable=True) 
    djia=sa.Column("djia", sa.Float, nullable=True) 
    
    def __init__(self, closing: dict = None): 
        self.Base = Base
        if "Date" in closing: self.date = closing["Date"]
        if "MMM" in closing: self.mmm = closing["MMM"]
        if "American Express" in closing: self.amex = closing["American Express"]
        if "Boeing" in closing: self.boeing = closing["Boeing"]
        if "Caterpillar" in closing: self.caterpillar = closing["Caterpillar"]
        if "Coca Cola" in closing: self.cocacola = closing["Coca Cola"]
        if "IBM" in closing: self.ibm = closing["IBM"]
        if "Intel" in closing: self.intel = closing["Intel"]
        if "J&J" in closing: self.jj = closing["J&J"]
        if "JPMorgan" in closing: self.jpmorgan = closing["JPMorgan"]
        if "McDonalds" in closing: self.mcdonalds = closing["McDonalds"]
        if "Merck & Co." in closing: self.merck = closing["Merck & Co."]
        if "Microsoft" in closing: self.microjunk = closing["Microsoft"]
        if "Procter & Gamble" in closing: self.procterandgamble = closing["Procter & Gamble"]
        if "The Home Depot" in closing: self.homedepot = closing["The Home Depot"]
        if "Walmart" in closing: self.walmart = closing["Walmart"]
        if "Walt Disney" in closing: self.waltdisney = closing["Walt Disney"]
        if "DJIA" in closing: self.djia = closing["DJIA"]
    
    SYMBOLS = [k for k in locals().keys() if not k.startswith('_')]

    def __repr__(self):
        return "<DJIA( {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} )>"\
                .format(
                    self.date, self.mmm, self.amex, self.boeing,
                    self.caterpillar, self.cocacola, self.ibm, self.intel,
                    self.jj, self.jpmorgan, self.mcdonalds, self.merck,
                    self.microjunk, self.procterandgamble, self.homedepot, self.walmart,
                    self.waltdisney,self.djia
                )



class DowJonesDB(object): 
    """Dow Jones database class with methods for connecting, table creation, insertion, etc.
    """ 
    def __init__(self, db_dict: dict = None) -> None: 
    
        db_url=db_dict["db_url"]
        db_server=db_dict["db_server"]
        db_table=db_dict["db_table"]
        db_port=db_dict["db_port"]
        db_user=db_dict["db_user"]
        db_password=db_dict["db_password"]
    
        db_uri = "postgresql://" + db_user + ':' + db_password + \
                 '@' + db_url + ':' + db_port + '/' + db_server
        
        self.tablename = db_table

        # connect to the database 
        self.engine, self.session = self.build_database_session(db_uri)
        if not self.table_exists():
            self.setup_database()
    
    def setup_database(self): 
        '''create a database at the given URI'''
        Base.metadata.create_all(self.engine) 
    
    
    def build_database_session(self, db_uri): 
        '''create an engine based on the config file'''
        engine = sa.create_engine(db_uri) 
        SessionBuilder = sessionmaker(bind=engine) 
        session = SessionBuilder() 
        return engine, session 
    
    
    def object_write(self, closing): 
        '''load an object’s values into the database''' 
        self.session.add(closing) 
    
     
    def object_bulk_write(self, closing_list): 
        '''load a list of objects into the database'''
        self.session.add_all(closing_list) 
    
    
    def query_closing(self, date): 
        '''determine whether an entry exists'''
        test = self.session.query(DJIA).filter(DJIA.date == date).first() 
        return test
    
     
    def table_exists(self): 
        '''check whether the table exists in the database'''
        insp = sa.inspect(self.engine) 
        exists = insp.has_table(self.tablename) 
        return exists


    def fetch_all_entries(self):
        closings = self.session.query(DJIA).all()
        for c in closings:
            print(c)
    

    def upsert(self, entry):
        table = entry.__table__
        stmt = insert(table)
        fields = [c for c in entry.SYMBOLS if c != table.primary_key]
        update_dict = {}
        for c in fields:
             update_dict[c] = getattr(entry, c)
        if not update_dict:
            raise ValueError("insert_or_update resulted in an empty update_dict")
        stmt = stmt.on_conflict_do_update(index_elements=table.primary_key, set_=update_dict)
        self.session.execute(stmt, update_dict)


    def commit_and_close(self):
        self.session.commit()
        print("Commiting changes ...")
        self.session.close()
        print("Closing session ...")
