#!/bin/bash

source /home/your_user_name/.secrets

HOSTDATA="/home/your_user_name/data"
IMAGENAME="stocks_etl"
TAG="0.0.1"

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$PG_URL \
    --env DB_SERVER=$PG_SERVER \
    --env DB_TABLE=djia \
    --env DB_PORT=$PG_PORT \
    --env DB_USER=$PG_USER \
    --env DB_PASSWORD=$PG_PASSWORD \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
