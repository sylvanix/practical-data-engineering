## A Containerized ETL example  


Why containerize our code? The primary reason is that the container will have the exact code and dependencies that we have tested with, so that anyone else running docker on their system, can use it and expect the same results.  


#### The ArangoDB database
From the [arango](./arango) directory, run this docker-compose command:  

```
~$ docker-compose -f docker-compose-arangodb.yml up -d
```

Now the database should be up. Let's look at the YAML file [docker-compose-arangodb.yml](./arango/docker-compose-arangodb.yml).  

```yaml
version: '3.7'
services:
    arangodb:
      image: arangodb:latest
      restart: unless-stopped
      ports:
        - 1234:8529
      environment:
        ARANGO_USER: root
        ARANGO_ROOT_PASSWORD: adbpwd
      volumes:
        - /home/sylvanix/volumes/arango-etl:/var/lib/arangodb3
```

One thing to note about docker-compose files, is that some arguments have a left-hand and a right-hand part, separated by a colon. The left-hand side value applies to the host (your physical machine), while the right-hand value applies to the container.  
For example, the value for ports is 1234:8529. The ArangoDB UI can be viewed at http://localhost:1234, though inside the container the port used by ArangoDB is 8529.  
The value for volumes is also comprised of two parts, with the left-hand side specifying where on the host the database files should be written.  

Navigate to the URL of the ArangoDB UI and sign in to the "_system" database with the username and password shown in the YAML file. Below, we'll add a database with and collection and insert some documents.  


#### The ETL 
ETL is for Extract, Transform, and Load. These operations are shown in the Python file [etl.py](./app/etl.py).  

```python
"""
An ETL (Extract, Transform, Load) example which reads in data from a source file, cleans it, and loads it in a database.
"""

import os
from pathlib import Path
import pandas as pd
from arango import ArangoClient


# Get the environment variables made available to the container
db_url = os.getenv("DB_URL")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")


#####################################################
# DATABASE CREATION & CONNECTION
####################################################
# Initialize the client for ArangoDB.
client = ArangoClient(hosts=db_url)

# Connect to the "_system" database as root user.
sys_db = client.db("_system", username=db_user, password=db_password)

# Create the "water" database if it does not exist
if not sys_db.has_database(db_name):
    sys_db.create_database(db_name)

# Connect to the "water" database as root user.
db = client.db(db_name, username=db_user, password=db_password)

# Create the "potability" collection if it does not exist
if db.has_collection('potability'):
    potability = db.collection('potability')
else:
    potability = db.create_collection('potability')


#####################################################
# ETL
####################################################
# (EXTRACT) Read the data from the csv
data_path = Path(data_dir) / "water_potability.csv"
df = pd.read_csv(data_path)

# (TRANSFORM) Handle the missing values
null_cols = ['ph','Sulfate','Trihalomethanes']
mask1 = df['Potability']==0
mask2 = df['Potability']==1
for col in null_cols:
    if col!='Sulfate':
        df[col].fillna(df[col].mean(), inplace=True)
    else:
        df.loc[mask1,col] = df.loc[mask1,col].fillna(df.loc[mask1, col].mean())
        df.loc[mask2,col] = df.loc[mask2,col].fillna(df.loc[mask2, col].mean())

# (LOAD) Write each record (row) to the database collection
dfstring = df.to_string(header=True,
                  index=False,
                  index_names=False).split('\n')
rows = [ele.split() for ele in dfstring]
header = rows[0]
rows = rows[1:]
row_dict = {}
for row in rows:
    for ii in range(len(header)):
        row_dict[header[ii]] = row[ii]
    # insert this row's values into the collection as a new document
    potability.insert(row_dict)
``` 

Hopefully the inline comments and your knowledge of Python make this code understandable. I admit that I usually have to revisit the Pandas documentation when I use dataframes :)  

To summarize, we read in the file [water_potability.csv](./water_quality/water_potability.csv), which I sourced [here](https://www.kaggle.com/code/gcmadhan/water-quality-prediction-76-h2o-80-accuracy/data).  Then, we filled replaced missing values with the column mean. Finally we inserted each row to the collection as a new document.  


#### Containerize the ETL  

The [Dockerfile](./Dockerfile) defines the container we want to build.  

```docker
# specify a base image
FROM python:slim-bullseye

# copy files to the new image
WORKDIR /workspace
COPY ./requirements.txt /workspace

# install Python modules
RUN python3 -m pip install --upgrade pip \
    && python3 -m pip install --no-cache-dir -r /workspace/requirements.txt

COPY ./app/ /workspace
RUN mkdir /data

# entrypoint
CMD ["python3", "etl.py"]
```
 
It specifies that we use a "slim" version of Debian GNU/Linux which contains a new-ish version of Python. A requirements.txt file defines what Python packages should be installed. The entrypoint command runs our etl code.  

The bash script [build_container.sh](./build_container.sh) pulls the base image and builds the container as we have specified it.  

```bash
#!/bin/bash

IMAGENAME="water_quality_etl"
TAG="0.0.1"

docker build -t $IMAGENAME:$TAG .
```

Run it with ```$ bash build_container.sh```  

The bash script [run_container.sh](./run_container.sh) starts the container and passes in some environment variables.  

```bash
#!/bin/bash

HOSTDATA="/home/sylvanix/1210SRD/Practical_Data_Engineering/containerized_etl/water_quality"
IMAGENAME="water_quality_etl"
TAG="0.0.1"
#CONTAINERNAME="etl_for_water_potability"

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=http://localhost:1234 \
    --env DB_NAME=water \
    --env DB_USER=root \
    --env DB_PASSWORD=adbpwd \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
```

Run it with ```$ bash run_container.sh```  

Look back at the ArangoDB UI and find the "water" database.  

![alt text](./images/potability_collection.png "the_collection")

Now click on one of the documents.  

![alt text](./images/document_in_collection.png "a_document")  
