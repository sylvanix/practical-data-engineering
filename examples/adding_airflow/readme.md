Here's an Airflow configuration with PostgreSQL backend, and a simple DAG test.


## Install

Move into the "services stack" dir.
```bash
cd services_stack
```

There are four files in the dir needed to bring up airflow: .env, airflow.cfg, docker-compose.airflow.yml, and services-stack  

.env

```bash
# AIRFLOW
AIRFLOW_UID=1000

# AIRFLOW POSTGRES
POSTGRES_USER=airflow
POSTGRES_PASSWORD=airflow
POSTGRES_DB=airflow
 
# Airflow Core
AIRFLOW__CORE__FERNET_KEY=8Hmc6kbnwsDnw7v3E8H1cUOydHZ-F1J5N-VCXPBj-o=
AIRFLOW__CORE__EXECUTOR=LocalExecutor
AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION=True
AIRFLOW__CORE__LOAD_EXAMPLES=False
AIRFLOW_UID=0
 
# Backend DB
AIRFLOW__DATABASE__SQL_ALCHEMY_CONN=postgresql+psycopg2://airflow:airflow@postgres/airflow
AIRFLOW__DATABASE__LOAD_DEFAULT_CONNECTIONS=False
 
# Airflow Init
_AIRFLOW_DB_UPGRADE=True
_AIRFLOW_WWW_USER_CREATE=True
_AIRFLOW_WWW_USER_USERNAME=airflow
_AIRFLOW_WWW_USER_PASSWORD=airflow
```

You can create your own Fernet key for the "AIRFLOW\_\_CORE\_\_FERNET" line above using the Cryptography module in Python.  

```bash
pip install cryptography
act_py311 (activating my virtual Python env)
python
>>> from cryptography.fernet import Fernet
>>> fernet_key = Fernet.generate_key()
>>> print(fernet_key.decode())

```

airflow.cfg  

```bash
[core]
 The home directory for Airflow
airflow_home = $PWD/airflow

# The executor class that Airflow should use. Choices include SequentialExecutor, LocalExecutor, and CeleryExecutor.
executor = LocalExecutor

[webserver]
# The base URL for your Airflow web server
base_url = http://localhost:8080

# The IP address to bind to
web_server_host = 0.0.0.0

# The port to bind to
web_server_port = 8080
```

docker-compose.airflow.yml

```bash
version: '3.4'

x-common:
  &common
  image: apache/airflow:2.7.1-python3.11
  user: "${AIRFLOW_UID}:0"
  env_file: 
    - $PWD/.env
  volumes:
    - $PWD/airflow/dags:/opt/airflow/dags
    - $PWD/airflow/logs:/opt/airflow/logs
    - $PWD/airflow/plugins:/opt/airflow/plugins
    - /var/run/docker.sock:/var/run/docker.sock

x-depends-on:
  &depends-on
  depends_on:
    postgres:
      condition: service_healthy
    airflow-init:
      condition: service_completed_successfully

services:
  postgres:
    image: postgres:15.4-bookworm
    container_name: airflow-postgres
    ports:
      - "5434:5432"
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "airflow"]
      interval: 5s
      retries: 5
    env_file:
      - $PWD/.env
    environment:
      - POSTGRES_USER=$POSTGRES_USER
      - POSTGRES_PASSWORD=$POSTGRES_PASSWORD
      - POSTGRES_DB=$POSTGRES_DB
    volumes:
      - $PWD/volumes/airflow/postgres:/var/lib/postgresql/data

  scheduler:
    <<: *common
    <<: *depends-on
    container_name: airflow-scheduler
    command: scheduler
    restart: on-failure
    ports:
      - "8793:8793"

  webserver:
    <<: *common
    <<: *depends-on
    container_name: airflow-webserver
    restart: always
    command: webserver
    ports:
      - "8080:8080"
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8080/health"]
      interval: 30s
      timeout: 30s
      retries: 5
  
  airflow-init:
    <<: *common
    container_name: airflow-init
    entrypoint: /bin/bash
    command:
      - -c
      - exec /entrypoint airflow version
```

services-stack

```bash
#!/bin/bash

# define variables
NETWORK_NAME=test-net

# create the network if it hasn't been already
NETWORK_CREATED=$(docker network ls | grep $NETWORK_NAME | wc -l)

[[ "$NETWORK_CREATED" == "1" ]] || docker network create $NETWORK_NAME


# create the following directories to prevent root from owning them
[[ "$1" == "up" ]] \
    && mkdir $PWD/airflow \
	&& mkdir $PWD/airflow/dags \
    && mkdir $PWD/airflow/logs \
    && mkdir $PWD/airflow/plugins \
    && mkdir $PWD/volumes \
    && mkdir -p $PWD/volumes/airflow/postgres


# bring up/down the services defined in the docker-compose files
docker-compose -f $PWD/docker-compose.airflow.yml \
               $@
```

I'm not using the 3 "NETWORK" entries in the file above, but will leave them there in case I need it later.  

To start the install and initial configuration:

```bash
./services-stack up -d
```

After a minute or two, the service should be available. Access the UI here: http://localhost:8080  

Look at the DAGs tab... pretty sparse. Let's add a DAG.  


## Add a test DAG
Copy the directory ```tests``` in the ```services_stack``` to ```services_stack/airflow/dags```  
After a while, the new DAG should appear in the UI. You can run the following command from the terminal to check whether the scheduler can yet see the new python file:

```bash
docker exec -it --user airflow airflow-scheduler bash -c "airflow dags list"
```

![alt text](./images/airflow-ui.png "Airflow UI")  

You can bring down the service with:

```bash
./services-stack down
```


Next, we'll bring up a Databases stack (SQL and NoSQL) and then we'll create an ETL tasks to run as a DAG from Airflow.  