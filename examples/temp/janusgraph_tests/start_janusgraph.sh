#!/bin/bash

source /home/sylvanix/.secrets
IMAGENAME="janusgraph/janusgraph"
TAG="1.0.0-20231017-051737.61faa25"

docker run \
    --name janusgraph-default \
    -d \
    $IMAGENAME:$TAG
