#!/bin/bash

IMAGENAME="janusgraph/janusgraph"
TAG="1.0.0-20231017-051737.61faa25"

docker run \
    --rm \
    --link janusgraph-default:janusgraph \
    -e GREMLIN_REMOTE_HOSTS=janusgraph \
    -it $IMAGENAME:$TAG \
    ./bin/gremlin.sh
