import pandas as pd
from PokemonDatabase import Pokemon, PokemonDB

def get_readings_from_file(filepath):
    entries = []
    #df = pd.read_csv(filepath, sep=',', skipinitialspace=True, quotechar='"')
    df = pd.read_csv(filepath, sep=',')
    header = [x.rstrip() for x in list(df.columns)]
    for row in df.itertuples():
        row = [str(x) for x in row[1:]]
        row_dict = {}
        for ii in range(len(header)):
            row_dict[header[ii]] = row[ii]
        bad_keys = []
        for k in row_dict:
            if row_dict[k] in ["nan", "?"]:
                bad_keys.append(k)
        for k in bad_keys:
            del row_dict[k]
        closing = Pokemon(row_dict)
        entries.append(closing)
    return entries


def load_database(entries, db):
    for row in entries:

        # fix the abilities string so that it's a comma-separated list in double quotes
        #print(row.abilities)
        abilities = row.abilities
        abilities = abilities.replace('[', '')
        abilities = abilities.replace(']', '')
        abilities = abilities.replace("'", "")
        #print(abilities, type(abilities))
        if ',' in abilities:
            abilities = abilities.split(',')
        else:
            abilities = [abilities]
        #print(abilities, type(abilities))
        row.abilities = list(abilities)
        #print(row.abilities)
        #print("")
        
        db.upsert(row)
