#!/bin/bash

docker run \
    --name age  \
    -p 5455:5432 \
    -e POSTGRES_USER=meowth \
    -e POSTGRES_PASSWORD=thatsright \
    -e POSTGRES_DB=agetest \
    -d \
    apache/age:PG15_latest

