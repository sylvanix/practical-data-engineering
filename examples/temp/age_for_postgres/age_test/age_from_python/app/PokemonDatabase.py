import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import insert, ARRAY
import psycopg2


Base = declarative_base()


class Pokemon(Base):
    """Pokemon dataset model class
    """ 
    __tablename__ = "pokemon" 
   
    abilities=sa.Column("abilities", ARRAY(sa.String))
    against_bug=sa.Column("against_bug", sa.Float)
    against_dark=sa.Column("against_dark", sa.Float)
    against_dragon=sa.Column("against_dragon", sa.Float)
    against_electric=sa.Column("against_electric", sa.Float)
    against_fairy=sa.Column("against_fairy", sa.Float)
    against_fight=sa.Column("against_fight", sa.Float)
    against_fire=sa.Column("against_fire", sa.Float)
    against_flying=sa.Column("against_flying", sa.Float)
    against_ghost=sa.Column("against_ghost", sa.Float)
    against_grass=sa.Column("against_grass", sa.Float)
    against_ground=sa.Column("against_ground", sa.Float)
    against_ice=sa.Column("against_ice", sa.Float)
    against_normal=sa.Column("against_normal", sa.Float)
    against_poison=sa.Column("against_poison", sa.Float)
    against_psychic=sa.Column("against_psychic", sa.Float)
    against_rock=sa.Column("against_rock", sa.Float)
    against_steel=sa.Column("against_steel", sa.Float)
    against_water=sa.Column("against_water", sa.Float)
    attack=sa.Column("attack", sa.Integer)
    base_egg_steps=sa.Column("base_egg_steps", sa.Integer)
    base_happiness=sa.Column("base_happiness", sa.Integer)
    base_total=sa.Column("base_total", sa.Integer)
    capture_rate=sa.Column("capture_rate", sa.String)
    classification=sa.Column("classification", sa.String)
    defense=sa.Column("defense", sa.Integer)
    experience_growth=sa.Column("experience_growth", sa.Integer)
    height_m=sa.Column("height_m", sa.Float)
    hp=sa.Column("hp", sa.Integer)
    japanese_name=sa.Column("japanese_name", sa.String)
    name=sa.Column("name", sa.String, nullable=False)
    percentage_male=sa.Column("percentage_male", sa.Float)
    pokedex_number=sa.Column("pokedex_number", sa.Integer, primary_key=True)
    sp_attack=sa.Column("sp_attack", sa.Integer)
    sp_defense=sa.Column("sp_defense", sa.Integer)
    speed=sa.Column("speed", sa.Integer)
    type1=sa.Column("type1", sa.String)
    type2=sa.Column("type2", sa.String)
    weight_kg=sa.Column("weight_kg", sa.Float)
    generation=sa.Column("generation", sa.Integer)
    is_legendary=sa.Column("is_legendary", sa.Integer)

 
    def __init__(self, entry: dict = None): 
        self.Base = Base
        if "abilities" in entry: self.abilities = entry["abilities"]
        if "against_bug" in entry: self.against_bug = entry["against_bug"]
        if "against_dark" in entry: self.against_dark = entry["against_dark"]
        if "against_dragon" in entry: self.against_dragon = entry["against_dragon"]
        if "against_electric" in entry: self.against_electric = entry["against_electric"]
        if "against_fairy" in entry: self.against_fairy = entry["against_fairy"]
        if "against_fight" in entry: self.against_fight = entry["against_fight"]
        if "against_fire" in entry: self.against_fire = entry["against_fire"]
        if "against_flying" in entry: self.against_flying = entry["against_flying"]
        if "against_ghost" in entry: self.against_ghost = entry["against_ghost"]
        if "against_grass" in entry: self.against_grass = entry["against_grass"]
        if "against_ground" in entry: self.against_ground = entry["against_ground"]
        if "against_ice" in entry: self.against_ice = entry["against_ice"]
        if "against_normal" in entry: self.against_normal = entry["against_normal"]
        if "against_poison" in entry: self.against_poison = entry["against_poison"]
        if "against_psychic" in entry: self.against_psychic = entry["against_psychic"]
        if "against_rock" in entry: self.against_rock = entry["against_rock"]
        if "against_steel" in entry: self.against_steel = entry["against_steel"]
        if "against_water" in entry: self.against_water = entry["against_water"]
        if "attack" in entry: self.attack = entry["attack"]
        if "base_egg_steps" in entry: self.base_egg_steps = entry["base_egg_steps"]
        if "base_happiness" in entry: self.base_happiness = entry["base_happiness"]
        if "base_total" in entry: self.base_total = entry["base_total"]
        if "capture_rate" in entry: self.capture_rate = entry["capture_rate"]
        if "classification" in entry: self.classification = entry["classification"]
        if "defense" in entry: self.defense = entry["defense"]
        if "experience_growth" in entry: self.experience_growth = entry["experience_growth"]
        if "height_m" in entry: self.height_m = entry["height_m"]
        if "hp" in entry: self.hp = entry["hp"]
        if "japanese_name" in entry: self.japanese_name = entry["japanese_name"]
        if "name" in entry: self.name = entry["name"]
        if "percentage_male" in entry: self.percentage_male = entry["percentage_male"]
        if "pokedex_number" in entry: self.pokedex_number = entry["pokedex_number"]
        if "sp_attack" in entry: self.sp_attack = entry["sp_attack"]
        if "sp_defense" in entry: self.sp_defense = entry["sp_defense"]
        if "speed" in entry: self.speed = entry["speed"]
        if "type1" in entry: self.type1 = entry["type1"]
        if "type2" in entry: self.type2 = entry["type2"]
        if "weight_kg" in entry: self.weight_kg = entry["weight_kg"]
        if "generation" in entry: self.generation = entry["generation"]
        if "is_legendary" in entry: self.is_legendary = entry["is_legendary"]
    
    SYMBOLS = [k for k in locals().keys() if not k.startswith('_')]


class PokemonDB(object): 
    """Pokemon database class with methods for connecting, table creation, insertion, etc.
    """ 
    def __init__(self, db_dict: dict = None) -> None: 
    
        db_url=db_dict["db_url"]
        db_server=db_dict["db_server"]
        db_table=db_dict["db_table"]
        db_port=db_dict["db_port"]
        db_user=db_dict["db_user"]
        db_password=db_dict["db_password"]
    
        db_uri = "postgresql://" + db_user + ':' + db_password + \
                 '@' + db_url + ':' + db_port + '/' + db_server
        
        self.tablename = db_table

        # connect to the database 
        self.engine, self.session = self.build_database_session(db_uri)
        if not self.table_exists():
            self.setup_database()
    
    def setup_database(self): 
        '''create a database at the given URI'''
        Base.metadata.create_all(self.engine) 
    
    
    def build_database_session(self, db_uri): 
        '''create an engine based on the config file'''
        engine = sa.create_engine(db_uri) 
        SessionBuilder = sessionmaker(bind=engine) 
        session = SessionBuilder() 
        return engine, session 
    
    
    def object_write(self, entry): 
        '''load an object’s values into the database''' 
        self.session.add(entry) 
    
     
    def object_bulk_write(self, entry_list): 
        '''load a list of objects into the database'''
        self.session.add_all(entry_list) 
    
    
    def query_entry(self, date): 
        '''determine whether an entry exists'''
        test = self.session.query(Pokemon).filter(Pokemon.date == date).first() 
        return test
    
     
    def table_exists(self): 
        '''check whether the table exists in the database'''
        insp = sa.inspect(self.engine) 
        exists = insp.has_table(self.tablename) 
        return exists


    def fetch_all_entries(self):
        entrys = self.session.query(Pokemon).all()
        return entrys
        #for c in entrys:
        #    print(c)
    

    def upsert(self, entry):
        table = entry.__table__
        stmt = insert(table)
        fields = [c for c in entry.SYMBOLS if c != table.primary_key]
        update_dict = {}
        for c in fields:
             update_dict[c] = getattr(entry, c)
        if not update_dict:
            raise ValueError("insert_or_update resulted in an empty update_dict")
        stmt = stmt.on_conflict_do_update(index_elements=table.primary_key, set_=update_dict)
        self.session.execute(stmt, update_dict)


    def commit_and_close(self):
        self.session.commit()
        print("Commiting changes ...")
        self.session.close()
        print("entry session ...")
