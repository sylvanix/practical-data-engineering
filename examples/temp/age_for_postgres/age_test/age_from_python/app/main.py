"""
An ETL (Extract, Transform, Load) example which reads in data from a source file, cleans it, and loads it in a database.
"""

import os
import age
from age.models import Vertex
from PokemonDatabase import PokemonDB


# Connect to the PostgreSQL database (running in a docker container)
db_url = os.getenv("DB_URL") 
db_server = os.getenv("DB_SERVER")
db_table = os.getenv("DB_TABLE")
db_port = os.getenv("DB_PORT")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")
db_dict = { 
    "db_url": db_url, "db_server": db_server, "db_table": db_table,
    "db_port": db_port, "db_user":db_user, "db_password": db_password
} 
db = PokemonDB(db_dict)


# Connect to the AGE service (running in docker container) 
age_host = "localhost"
age_port = os.getenv("AGE_PORT")
age_db = os.getenv("AGE_DB")
age_user = os.getenv("AGE_USER")
age_password = os.getenv("AGE_PASSWORD")
age_graph_name = "test_graph"

print("Connecting to Test Graph.....")
ag = age.connect(
    graph=age_graph_name,
    host=age_host,
    port=age_port,
    dbname=age_db,
    user=age_user,
    password=age_password
)


entries = db.fetch_all_entries()
for e in entries:
    print(e.pokedex_number, e.name)
