#!/bin/bash

source /home/sylvanix/.secrets

IMAGENAME="age_graphs_pokemon"
TAG="0.0.1"

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=$PG_URL \
    --env DB_SERVER=$PG_SERVER \
    --env DB_TABLE=agetest \
    --env DB_PORT=$PG_PORT \
    --env DB_USER=$PG_USER \
    --env DB_PASSWORD=$PG_PASSWORD \
    --env AGE_USER=$AGE_USER \
    --env AGE_PASSWORD=$AGE_PASSWORD \
    --env AGE_DB=$AGE_DB \
    --env AGE_PORT=$AGE_PORT \
    $IMAGENAME:$TAG
