#!/bin/bash

HOSTDATA="/home/sylvanix/PropitiousIO/datavolume/Dow_Jones"
IMAGENAME="stocks_etl"
TAG="0.0.1"

docker build -t $IMAGENAME:$TAG .

docker run \
    --net=host \
    --user=1000:1000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DB_URL=127.0.0.1 \
    --env DB_SERVER=stocks \
    --env DB_TABLE=djia \
    --env DB_PORT=5432 \
    --env DB_USER=sylvanix \
    --env DB_PASSWORD=passwd \
    --env DATA_DIR=/data \
    $IMAGENAME:$TAG
