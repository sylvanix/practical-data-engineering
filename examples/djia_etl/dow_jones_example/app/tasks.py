import pandas as pd
from DowJonesDatabase import DJIA, DowJonesDB

def get_readings_from_file(filepath):
    closings = []
    df = pd.read_csv(filepath)
    header = [x.rstrip() for x in list(df.columns)]
    for row in df.itertuples():
        row = [str(x) for x in row[1:]]
        row_dict = {}
        for ii in range(len(header)):
            row_dict[header[ii]] = row[ii]
        bad_keys = []
        for k in row_dict:
            if row_dict[k] in ["nan", "?"]:
                bad_keys.append(k)
        for k in bad_keys:
            del row_dict[k]
        closing = DJIA(row_dict)
        closings.append(closing)
    return closings


def load_database(closings, db):
    for row in closings:
        #db.object_write(row)
        #exists = db.query_closing(row.date)
        #db.fetch_all_entries()            
        #db.object_update_or_add(row)
		db.object_upsert(row)
