"""
An ETL (Extract, Transform, Load) example which reads in data from a source file, cleans it, and loads it in a database.
"""

import os
from pathlib import Path
import pandas as pd
import psycopg2
import tasks
from DowJonesDatabase import DowJonesDB


# Get the environment variables made available to the container
db_url = os.getenv("DB_URL") 
db_server = os.getenv("DB_SERVER")
db_table = os.getenv("DB_TABLE")
db_port = os.getenv("DB_PORT")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
data_dir = os.getenv("DATA_DIR")
db_dict = { 
    "db_url": db_url, "db_server": db_server, "db_table": db_table,
    "db_port": db_port, "db_user":db_user, "db_password": db_password
} 

db = DowJonesDB(db_dict)

filepath = Path(data_dir) / "Dow_Jones_Historical_Data.csv"
readings = tasks.get_readings_from_file(filepath)

tasks.load_database(readings, db)

db.commit_and_close()
