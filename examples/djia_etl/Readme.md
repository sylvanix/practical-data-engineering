## DJIA ETL Example
Another ETL example, a larger services stack.


#### The Dataset  
We'll be working with data from the following Kaggle dataset:  [Dow Jones - Historic Stock Data 2000-2020](https://www.kaggle.com/code/deeplytics/dow-jones-historic-stock-data-2000-2020/data).  

Clone this reposiory. Download the dataset and place the .csv in the ```datavolume/Dow_Jones``` directory. As usual there will be some missing data, but our database class will be able to handle those instances.   


