## Using MLflow  


in the mlflow_server dir:  

change the path to the mlruns directory in the /ml-flow-docker/Dockerfile. You'll see that I am using ```/home/sylvanix/data/mlruns```; change this to the path of a directory on your system.  

Now start the MLflow container:  
```Bash
docker-compose -f docker-compose.yml up -d
```

Now you can run the ["Jupyter Notebook Example"](./notebook_example) and the ["Dockerized-Example"](./dockerized_example).  

In both examples, you'll need to substitute my filepaths to ones on your system.  