#!/bin/bash

# build
IMAGENAME="containerized_mlflow"
TAG="test"
docker build -t $IMAGENAME:$TAG .

# run
HOSTDATA="/home/sylvanix/data"
EXPNAME="yet-another-test"
docker run \
    --net=host \
    -v /etc/localtime:/etc/localtime:ro \
    -v $HOSTDATA:/data \
    --env PYTHONBUFFERED=1 \
    --env DATA_DIR=/data \
    --env EXPERIMENT_NAME=${EXPNAME} \
    $IMAGENAME:$TAG
